import { Component, OnInit } from '@angular/core';
import { TypeService } from '../type.service';
import { ActivatedRoute } from '@angular/router';
import { Notice } from '../models/notice.model';
import { Typee } from '../models/type.mode';

@Component({
  selector: 'app-notice-of-type',
  templateUrl: './notice-of-type.component.html',
  styleUrls: ['./notice-of-type.component.css']
})
export class NoticeOfTypeComponent implements OnInit {

  sub:any;
  id1:number;
  id2:number;
  types:Typee[];
  notices:Notice[];
  constructor(private route:ActivatedRoute,  private typeSerice:TypeService) {
    route.params.subscribe(val => {
      this.getNoticesOfSelectedType();
      this.getTypesOnlyFromSelectedGroup();
    });
   }

  ngOnInit() {
 
  }

  getTypesOnlyFromSelectedGroup(){
    this.sub=this.route.params.subscribe(params=>{
      this.id1 =+params['id1'];
      this.typeSerice.getTypeOnlyOfThatGroup(this.id1).subscribe((res:Typee[])=>{
      this.types=res;
    })
  })
}

  // ova dva parametra preuzimamo iz putanje koju smo dobili kad smo u group-page sistnuli tip, iz putanje group/id1/id2, preuzimamo parametre
  // id1 je ustvari Grupin id, dok je Id2 id od tipa koji smo selektovali

  getNoticesOfSelectedType(){
    this.sub= this.route.params.subscribe(params=>{
      this.id1=+params['id1'];
      this.id2=+params['id2'];
      this.typeSerice.getNoticesOfSelectedType(this.id1,this.id2).subscribe((res:Notice[])=>{
        this.notices=res;
      })
    })
  }

}
