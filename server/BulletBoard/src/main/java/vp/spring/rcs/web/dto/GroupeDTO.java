package vp.spring.rcs.web.dto;

import java.util.HashSet;
import java.util.Set;

import vp.spring.rcs.model.Groupe;
import vp.spring.rcs.model.Notice;

public class GroupeDTO {

	
Long id;
	
	String name;
	
	Set<KorisnikDTO> users=new HashSet<>();
	Set<NoticeDTO> notices=new HashSet<>();
	
	public GroupeDTO(Groupe groupe) {
		this.id=groupe.getId();
		this.name=groupe.getName();
	}
	
	


	public Set<NoticeDTO> getNotices() {
		return notices;
	}




	public void setNotices(Set<NoticeDTO> notices) {
		this.notices = notices;
	}




	public GroupeDTO() {
		super();
	}




	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Set<KorisnikDTO> getUsers() {
		return users;
	}


	public void setUsers(Set<KorisnikDTO> users) {
		this.users = users;
	}
	
	
	
}
