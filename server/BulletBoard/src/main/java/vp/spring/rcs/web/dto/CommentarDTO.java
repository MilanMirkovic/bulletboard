package vp.spring.rcs.web.dto;

import vp.spring.rcs.model.Commentar;

public class CommentarDTO {

	
Long id;
	
	String commentar;
	NoticeDTO  notice;
	KorisnikDTO user;
	String datum;
	int parentComment;
	public CommentarDTO(Commentar commentar) {
		this.id=commentar.getId();
		this.parentComment=commentar.getParentComment();
		this.commentar=commentar.getCommentar();
		this.notice=new NoticeDTO(commentar.getNotice());
		this.user=new KorisnikDTO(commentar.getUser());
		this.datum=commentar.getDatum();
	}
	
	
	
	

	public int getParentComment() {
		return parentComment;
	}





	public void setParentComment(int parentComment) {
		this.parentComment = parentComment;
	}





	public CommentarDTO() {
		super();
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommentar() {
		return commentar;
	}

	public void setCommentar(String commentar) {
		this.commentar = commentar;
	}

	public NoticeDTO getNotice() {
		return notice;
	}

	public void setNotice(NoticeDTO notice) {
		this.notice = notice;
	}

	public KorisnikDTO getUser() {
		return user;
	}

	public void setUser(KorisnikDTO user) {
		this.user = user;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}
	
	
	
}
