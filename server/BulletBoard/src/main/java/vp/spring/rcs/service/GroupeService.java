package vp.spring.rcs.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.GroupeRepository;
import vp.spring.rcs.model.Groupe;
import vp.spring.rcs.model.Notice;
import vp.spring.rcs.model.Type;
import vp.spring.rcs.model.user.SecurityUser;

@Component
public class GroupeService {

	@Autowired
	GroupeRepository groupeRepo;

	@Autowired
	NoticeService noticeService;

	@Autowired
	TypeService typeService;

	public List<Groupe> findAll() {
		return groupeRepo.findAll();
	}

	public Groupe findOne(Long id) {
		return groupeRepo.getOne(id);
	}

	public Groupe findByName(String name) {
		return groupeRepo.findByName(name);
	}

	//Uzima obestenja samo u toj grupi u kojoj se nalazimo npr("prvi sprat", "majstori")
	public List<Notice> NoticeOfSelectedGroupe(Long id) {
		Groupe groupe = findOne(id);                 //prvo nadje grupu po id
		List<Notice> notices = new ArrayList<>();  //lista za skladistenje obavestenja koja se filtiraraju u sledeceoj petlji
		for (Notice n : groupe.getNotices()) { // prolazi kroz sve korisnike te grupe koju smo pronasli preko ID, uzima korisnikove objave i skladisti u notice
		
				notices.add(n);

			
		}

		return notices;
	}

	//Uzima tipove samo za tu gurpu u kojoj se nalazimo
	public Set<Type> TypesOfOneGroup(Long id) {
		List<Type> types = typeService.findAll(); 
		List<Notice> notices = NoticeOfSelectedGroupe(id);
		Set<Type> typeForThatGroupOnly = new HashSet<>();
		for (Type t : types) {  // prolazi kroz listu svih tipova(hitno,slavlje..) ii zatim prolazi kroz list
			for (Notice n : notices) {	// zatim prolazi kroz list  vec filtriranih obavestenja koja smo radili gore, i poredi ID od Tip i ID od Tipa
				if (t.getId() == n.getType().getId()) {  //koja se nalazi u notices ako se ID poklapaju smetamo ih u typeForGroupsOnly
					typeForThatGroupOnly.add(t);  //koristimo Set listu jer ona ne sadrzi duplikate
				}
			}

		}

		return typeForThatGroupOnly;
	}



	
	
//	public List<Notice> noticesOfType(Long id) {
//
//		List<Notice> notice = NoticeOfSelectedGroupe(id);
//
//		Set<Type> types = TypesOfOneGroup(id);
//		List<Notice> retVal = new ArrayList<>();
//
//		for (Notice n : notice) {
//
//			if (n.getType().getId() == id) {
//				retVal.add(n);
//
//			}
//		}
//
//		return retVal;
//
//}
	
	//filtrira beleske u odredjenoj grupi/ po tipu koji izaberemo/ i izlista beleske tog tipa samo za tu grupu a ne za sve grupe
	
	public List<Notice> noticesOfType(Long groupId,Long id) {

		//dobavlja beleske odredjene grupe
		List<Notice> notice = NoticeOfSelectedGroupe(groupId);
		//dobavlja tipove samo te  grupe u kojoj se nalazi
		Set<Type> types = TypesOfOneGroup(groupId);
		
		List<Notice> retVal = new ArrayList<>();

		//filtrila beleske samo tog tipa za samo tu grupu, prolazi kroz listu beleski koju smo gore filtrirari samo za tu grupu u kojoj smo
		for (Notice n : notice) {

			if (n.getType().getId() == id) {
				retVal.add(n);

			}
		}

		return retVal;

}

	public List<SecurityUser> usersOfGroup(Long id) {
		Groupe groupe = findOne(id);

		List<SecurityUser> usersOfGroup = new ArrayList<>();
		for (SecurityUser s : groupe.getUsers()) {
			usersOfGroup.add(s);
		}

		return usersOfGroup;
	}
}
