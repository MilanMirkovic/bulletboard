package vp.spring.rcs.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import  vp.spring.rcs.model.Type;

@Component
public interface TypeRepository extends JpaRepository<Type,Long> {

}
