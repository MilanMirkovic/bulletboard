import { Component, OnInit,Input } from '@angular/core';


import { TypeService } from '../type.service';
import {  Typee } from '../models/type.mode';

@Component({
  selector: 'app-kategorije-naviagation-bar',
  templateUrl: './kategorije-naviagation-bar.component.html',
  styleUrls: ['./kategorije-naviagation-bar.component.css']
})
export class KategorijeNaviagationBarComponent implements OnInit {


  @Input() 
  types:Typee[];
  constructor(private typeService:TypeService) { }

  ngOnInit() {
    this.getTypes();
  }

  
  getTypes(){
    this.typeService.getAll().subscribe((res:Typee[])=>{
      this.types=res;
    })

  }


}
