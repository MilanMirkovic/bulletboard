(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _groupe_page_groupe_page_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./groupe-page/groupe-page.component */ "./src/app/groupe-page/groupe-page.component.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _notice_of_type_notice_of_type_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./notice-of-type/notice-of-type.component */ "./src/app/notice-of-type/notice-of-type.component.ts");
/* harmony import */ var _notice_details_notice_details_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./notice-details/notice-details.component */ "./src/app/notice-details/notice-details.component.ts");
/* harmony import */ var _filter_notices_filter_notices_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./filter-notices/filter-notices.component */ "./src/app/filter-notices/filter-notices.component.ts");









var routes = [
    // { path: 'record/:id', component: RecordDetailsComponent, canActivate:[CanActivateAuthGuard] },
    { path: 'notices/:name', component: _filter_notices_filter_notices_component__WEBPACK_IMPORTED_MODULE_8__["FilterNoticesComponent"] },
    { path: 'notice/:id', component: _notice_details_notice_details_component__WEBPACK_IMPORTED_MODULE_7__["NoticeDetailsComponent"] },
    { path: 'main', component: _main_main_component__WEBPACK_IMPORTED_MODULE_5__["MainComponent"] },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'group/:id', component: _groupe_page_groupe_page_component__WEBPACK_IMPORTED_MODULE_4__["GroupePageComponent"] },
    { path: 'group/:id1/:id2', component: _notice_of_type_notice_of_type_component__WEBPACK_IMPORTED_MODULE_6__["NoticeOfTypeComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { onSameUrlNavigation: 'reload' })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <nav class=\"navbar navbar-inverse navbar-fixed-top\">\r\n\t<div class=\"container\">\r\n\t\t<div class=\"navbar-header\">\r\n\t\t\t<button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">\r\n\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>\r\n\t\t\t\t<span class=\"icon-bar\"></span>\r\n\t\t\t\t<span class=\"icon-bar\"></span>\r\n\t\t\t\t<span class=\"icon-bar\"></span>\r\n\t\t\t</button>\r\n\t\t\t<a class=\"navbar-brand\" href=\"#\">RecordStore</a>\r\n\t\t</div>\r\n\t\t<div id=\"navbar\" class=\"navbar-collapse collapse\">\r\n\t\t\t<ul class=\"nav navbar-nav\">\r\n\t\t\t\t<li class=\"active\"><a href=\"#\">Home</a></li>\r\n\t\t\t\t<li><a href=\"#about\">About</a></li>\r\n\t\t\t\t<li><a href=\"#contact\">Contact</a></li>\r\n\t\t\t</ul>\r\n\t\t\t<ul class=\"nav navbar-nav pull-right\">\r\n\t\t\t\t<li *ngIf=\"isLoggedIn()\"><a (click)=\"logout()\">Logout</a></li>\r\n\t\t\t</ul>\r\n\t\t</div>\r\n\t</div>\r\n</nav>\r\n<div class=\"container\" role=\"main\">\r\n\t\r\n\t<div class=\"jumbotron\">\r\n\t\t<h1>RecordStore</h1>\r\n\t\t<p>An example of Angular application styled with Bootstrap.</p>\r\n\t</div>\r\n\r\n\t<router-outlet></router-outlet>\r\n\r\n</div> -->\r\n\r\n<app-group-nav-bar></app-group-nav-bar>\r\n\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./security/authentication.service */ "./src/app/security/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
    }
    AppComponent.prototype.logout = function () {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    };
    AppComponent.prototype.isLoggedIn = function () {
        return this.authenticationService.isLoggedIn();
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./page-not-found/page-not-found.component */ "./src/app/page-not-found/page-not-found.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./security/authentication.service */ "./src/app/security/authentication.service.ts");
/* harmony import */ var _security_can_activate_auth_guard__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./security/can-activate-auth.guard */ "./src/app/security/can-activate-auth.guard.ts");
/* harmony import */ var _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./security/token-interceptor.service */ "./src/app/security/token-interceptor.service.ts");
/* harmony import */ var _security_jwt_utils_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./security/jwt-utils.service */ "./src/app/security/jwt-utils.service.ts");
/* harmony import */ var _kategorije_naviagation_bar_kategorije_naviagation_bar_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./kategorije-naviagation-bar/kategorije-naviagation-bar.component */ "./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.ts");
/* harmony import */ var _group_nav_bar_group_nav_bar_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./group-nav-bar/group-nav-bar.component */ "./src/app/group-nav-bar/group-nav-bar.component.ts");
/* harmony import */ var _groupe_page_groupe_page_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./groupe-page/groupe-page.component */ "./src/app/groupe-page/groupe-page.component.ts");
/* harmony import */ var _main_main_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./main/main.component */ "./src/app/main/main.component.ts");
/* harmony import */ var _notice_of_type_notice_of_type_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./notice-of-type/notice-of-type.component */ "./src/app/notice-of-type/notice-of-type.component.ts");
/* harmony import */ var _notice_details_notice_details_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./notice-details/notice-details.component */ "./src/app/notice-details/notice-details.component.ts");
/* harmony import */ var _filter_notices_filter_notices_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./filter-notices/filter-notices.component */ "./src/app/filter-notices/filter-notices.component.ts");





















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _page_not_found_page_not_found_component__WEBPACK_IMPORTED_MODULE_7__["PageNotFoundComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
                _kategorije_naviagation_bar_kategorije_naviagation_bar_component__WEBPACK_IMPORTED_MODULE_13__["KategorijeNaviagationBarComponent"],
                _group_nav_bar_group_nav_bar_component__WEBPACK_IMPORTED_MODULE_14__["GroupNavBarComponent"],
                _groupe_page_groupe_page_component__WEBPACK_IMPORTED_MODULE_15__["GroupePageComponent"],
                _main_main_component__WEBPACK_IMPORTED_MODULE_16__["MainComponent"],
                _notice_of_type_notice_of_type_component__WEBPACK_IMPORTED_MODULE_17__["NoticeOfTypeComponent"],
                _notice_details_notice_details_component__WEBPACK_IMPORTED_MODULE_18__["NoticeDetailsComponent"],
                _filter_notices_filter_notices_component__WEBPACK_IMPORTED_MODULE_19__["FilterNoticesComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"]
            ],
            providers: [
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                    useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_11__["TokenInterceptorService"],
                    multi: true
                },
                _security_authentication_service__WEBPACK_IMPORTED_MODULE_9__["AuthenticationService"],
                _security_can_activate_auth_guard__WEBPACK_IMPORTED_MODULE_10__["CanActivateAuthGuard"],
                _security_jwt_utils_service__WEBPACK_IMPORTED_MODULE_12__["JwtUtilsService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/filter-notices/filter-notices.component.css":
/*!*************************************************************!*\
  !*** ./src/app/filter-notices/filter-notices.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\r\n    list-style-type: none;\r\n    margin-bottom: 140px;\r\n    border: 3px black;\r\n  }\r\n\r\n span{\r\n   font-style: italic;\r\n   font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif\r\n }\r\n\r\n .type{\r\n    float: left;\r\n  }\r\n\r\n .date{\r\n    float: right;\r\n  }\r\n\r\n .username{\r\n    float: left;\r\n  }\r\n\r\n h5{\r\n    margin-bottom: 30px;\r\n  }\r\n\r\n hr{\r\n   color: black;\r\n   height: 2px;\r\n   \r\n }\r\n \r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmlsdGVyLW5vdGljZXMvZmlsdGVyLW5vdGljZXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHFCQUFxQjtJQUNyQixvQkFBb0I7SUFDcEIsaUJBQWlCO0VBQ25COztDQUVEO0dBQ0Usa0JBQWtCO0dBQ2xCO0NBQ0Y7O0NBRUE7SUFDRyxXQUFXO0VBQ2I7O0NBRUE7SUFDRSxZQUFZO0VBQ2Q7O0NBQ0E7SUFDRSxXQUFXO0VBQ2I7O0NBRUY7SUFDSSxtQkFBbUI7RUFDckI7O0NBRUY7R0FDRyxZQUFZO0dBQ1osV0FBVzs7Q0FFYiIsImZpbGUiOiJzcmMvYXBwL2ZpbHRlci1ub3RpY2VzL2ZpbHRlci1ub3RpY2VzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ1bCB7XHJcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxNDBweDtcclxuICAgIGJvcmRlcjogM3B4IGJsYWNrO1xyXG4gIH1cclxuXHJcbiBzcGFue1xyXG4gICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgIGZvbnQtZmFtaWx5OiAnR2lsbCBTYW5zJywgJ0dpbGwgU2FucyBNVCcsIENhbGlicmksICdUcmVidWNoZXQgTVMnLCBzYW5zLXNlcmlmXHJcbiB9XHJcblxyXG4gLnR5cGV7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICB9XHJcbiBcclxuICAuZGF0ZXtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICB9XHJcbiAgLnVzZXJuYW1le1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgfVxyXG4gXHJcbmg1e1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiBcclxuaHJ7XHJcbiAgIGNvbG9yOiBibGFjaztcclxuICAgaGVpZ2h0OiAycHg7XHJcbiAgIFxyXG4gfVxyXG4gXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/filter-notices/filter-notices.component.html":
/*!**************************************************************!*\
  !*** ./src/app/filter-notices/filter-notices.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n      <div >\n        <ul *ngFor=\"let n of notices\">\n          <li>\n            <div>\n              <span> <a [routerLink]=\"['/notice', n.id]\">\n                  <h3>{{n.title}}</h3>\n                </a></span>\n              <h5>{{n.text}}</h5>\n              <div>\n                <span>\n                  <p class=\"username\"> By: {{n.user.username}}</p>\n                  <p class=\"date\">Date: {{n.date}}</p>\n                </span>\n                <br>\n                <p class=\"type\">Tip : {{n.type.name}}</p>\n  \n              </div>\n            </div>\n            <hr>\n          </li>\n          <hr>\n        </ul>\n  \n        <hr>\n      </div>\n      <div>"

/***/ }),

/***/ "./src/app/filter-notices/filter-notices.component.ts":
/*!************************************************************!*\
  !*** ./src/app/filter-notices/filter-notices.component.ts ***!
  \************************************************************/
/*! exports provided: FilterNoticesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilterNoticesComponent", function() { return FilterNoticesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _notice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../notice.service */ "./src/app/notice.service.ts");




var FilterNoticesComponent = /** @class */ (function () {
    function FilterNoticesComponent(route, noticeService) {
        this.route = route;
        this.noticeService = noticeService;
    }
    FilterNoticesComponent.prototype.ngOnInit = function () {
        this.getNotices();
    };
    FilterNoticesComponent.prototype.getNotices = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.name = params['name'];
            _this.noticeService.findByNoticeName(_this.name).subscribe(function (res) {
                _this.notices = res;
            });
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], FilterNoticesComponent.prototype, "notices", void 0);
    FilterNoticesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-filter-notices',
            template: __webpack_require__(/*! ./filter-notices.component.html */ "./src/app/filter-notices/filter-notices.component.html"),
            styles: [__webpack_require__(/*! ./filter-notices.component.css */ "./src/app/filter-notices/filter-notices.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _notice_service__WEBPACK_IMPORTED_MODULE_3__["NoticeService"]])
    ], FilterNoticesComponent);
    return FilterNoticesComponent;
}());



/***/ }),

/***/ "./src/app/group-nav-bar/group-nav-bar.component.css":
/*!***********************************************************!*\
  !*** ./src/app/group-nav-bar/group-nav-bar.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar .navbar-nav {\r\n    display: inline-block;\r\n    float: none;\r\n    vertical-align: top;\r\n  }\r\n  \r\n  .navbar .navbar-collapse {\r\n    text-align: center;\r\n  }\r\n  \r\n  img{\r\n   \r\n      \r\n      align-content: center;\r\n  }\r\n  \r\n  form{\r\n      float:rigth;\r\n  }\r\n  \r\n  .search-container {\r\n   \r\n    float: right;\r\n    padding: 6px 10px;\r\n    font-size: 17px;\r\n    cursor: pointer;\r\n  }\r\n  \r\n  #logOut{\r\n    float: right;\r\n  }\r\n  \r\n  .welcome{\r\n    float: left;\r\n    color: white;\r\n    size: 0.5em;\r\n  \r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZ3JvdXAtbmF2LWJhci9ncm91cC1uYXYtYmFyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLG1CQUFtQjtFQUNyQjs7RUFFQTtJQUNFLGtCQUFrQjtFQUNwQjs7RUFFQTs7O01BR0kscUJBQXFCO0VBQ3pCOztFQUNBO01BQ0ksV0FBVztFQUNmOztFQUVBOztJQUVFLFlBQVk7SUFDWixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGVBQWU7RUFDakI7O0VBQ0E7SUFDRSxZQUFZO0VBQ2Q7O0VBRUE7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLFdBQVc7O0VBRWIiLCJmaWxlIjoic3JjL2FwcC9ncm91cC1uYXYtYmFyL2dyb3VwLW5hdi1iYXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uYXZiYXIgLm5hdmJhci1uYXYge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgZmxvYXQ6IG5vbmU7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gIH1cclxuICBcclxuICAubmF2YmFyIC5uYXZiYXItY29sbGFwc2Uge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgaW1ne1xyXG4gICBcclxuICAgICAgXHJcbiAgICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcclxuICB9XHJcbiAgZm9ybXtcclxuICAgICAgZmxvYXQ6cmlndGg7XHJcbiAgfVxyXG5cclxuICAuc2VhcmNoLWNvbnRhaW5lciB7XHJcbiAgIFxyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgcGFkZGluZzogNnB4IDEwcHg7XHJcbiAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG4gICNsb2dPdXR7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgfVxyXG5cclxuICAud2VsY29tZXtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgc2l6ZTogMC41ZW07XHJcbiAgXHJcbiAgfSJdfQ== */"

/***/ }),

/***/ "./src/app/group-nav-bar/group-nav-bar.component.html":
/*!************************************************************!*\
  !*** ./src/app/group-nav-bar/group-nav-bar.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n    <img src=\"https://bowergroupasia.com/wp-content/themes/radiant/library/images/skylines/australia.png\" class=\"img-responsive center-block\" alt=\"Responsive image\">\n</div>\n<nav class=\"navbar navbar-inverse  \"  role=\"navigation\" align=\"center\">\n \n  \n  <div class=\"container-fluid\">\n    <!-- Brand and toggle get grouped for better mobile display -->\n    <div class=\"navbar-header\">\n      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\n        <span class=\"sr-only\">Toggle navigation</span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n        <span class=\"icon-bar\"></span>\n      </button>\n      <p class=\"welcome  navbar-brand\" *ngIf=\"isLoggedIn()\">Welcome: {{korisnikUsername}} !</p>\n      <a class=\"navbar-brand\" [routerLink]=\"['main']\" href=\"#\">Home</a>\n      \n     \n    </div>\n  \n    <!-- Collect the nav links, forms, and other content for toggling -->\n    <div   class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n      <ul class=\"nav navbar-nav\" *ngFor=\"let t of groups\">\n        <!-- prosledjuje od grupe id i baca na stranicu GroupPage, koja sadrzi samo obavestenja iz te grupe-->\n        <li   *ngIf=\"isLoggedIn()\" class=\"pull-left\"><a [routerLink]=\"['/group', t.id]\">{{t.name}}</a></li>\n        \n\n          \n      </ul>\n      <a (click)=\"logout()\"> <span class=\"navbar-brand\" id=\"logOut\" *ngIf=\"isLoggedIn()\">LogOut</span></a>\n     <a [routerLink]=\"['login']\"><span class=\"navbar-brand\" id=\"logOut\" *ngIf=\"!isLoggedIn()\">LogIn</span></a>\n         \n      <div class=\"search-container\">\n              <form >\n                <input type=\"text\" placeholder=\"Search for Notices..\" name=\"naziv\" [(ngModel)]=\"name\">\n               <a [routerLink]=\"['notices/', name]\"> <button type=\"submit\"><i class=\"fa fa-search\" >submit</i></button></a>\n              </form>\n            </div>\n    </div><!-- /.navbar-collapse -->\n\n  </div><!-- /.container-fluid -->\n</nav>"

/***/ }),

/***/ "./src/app/group-nav-bar/group-nav-bar.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/group-nav-bar/group-nav-bar.component.ts ***!
  \**********************************************************/
/*! exports provided: GroupNavBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupNavBarComponent", function() { return GroupNavBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _type_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../type.service */ "./src/app/type.service.ts");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../security/authentication.service */ "./src/app/security/authentication.service.ts");
/* harmony import */ var _notice_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../notice.service */ "./src/app/notice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var GroupNavBarComponent = /** @class */ (function () {
    function GroupNavBarComponent(typeService, atuhetntication, noticeService, router) {
        this.typeService = typeService;
        this.atuhetntication = atuhetntication;
        this.noticeService = noticeService;
        this.router = router;
    }
    GroupNavBarComponent.prototype.ngOnInit = function () {
        this.getTypes();
        this.getUserName();
        this.completeUser();
    };
    GroupNavBarComponent.prototype.getTypes = function () {
        var _this = this;
        this.typeService.getAll().subscribe(function (res) {
            _this.types = res;
        });
    };
    GroupNavBarComponent.prototype.getUserName = function () {
        this.korisnikUsername = this.atuhetntication.getUser();
    };
    // vraca samo one grupe koji je trenutno ulogovani user clan
    GroupNavBarComponent.prototype.completeUser = function () {
        var _this = this;
        this.noticeService.getUserGroup(this.korisnikUsername).subscribe(function (rets) {
            _this.groups = rets;
        });
    };
    GroupNavBarComponent.prototype.logout = function () {
        this.atuhetntication.logout();
        this.router.navigate(['/login']);
    };
    GroupNavBarComponent.prototype.isLoggedIn = function () {
        return this.atuhetntication.isLoggedIn();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], GroupNavBarComponent.prototype, "types", void 0);
    GroupNavBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-group-nav-bar',
            template: __webpack_require__(/*! ./group-nav-bar.component.html */ "./src/app/group-nav-bar/group-nav-bar.component.html"),
            styles: [__webpack_require__(/*! ./group-nav-bar.component.css */ "./src/app/group-nav-bar/group-nav-bar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_type_service__WEBPACK_IMPORTED_MODULE_2__["TypeService"], _security_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"], _notice_service__WEBPACK_IMPORTED_MODULE_4__["NoticeService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], GroupNavBarComponent);
    return GroupNavBarComponent;
}());



/***/ }),

/***/ "./src/app/groupe-page/groupe-page.component.css":
/*!*******************************************************!*\
  !*** ./src/app/groupe-page/groupe-page.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.lista {\r\n  list-style-type: none;\r\n  margin-bottom: 140px;\r\n  border: 3px black;\r\n}\r\n\r\n span{\r\n   font-style: italic;\r\n   font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif\r\n }\r\n\r\n .type{\r\n   float: left;\r\n }\r\n\r\n .date{\r\n   float: right;\r\n }\r\n\r\n .username{\r\n   float: left;\r\n }\r\n\r\n h5{\r\n   margin-bottom: 30px;\r\n }\r\n\r\n hr{\r\n  color: black;\r\n  \r\n}\r\n\r\n ul{\r\n  list-style-type: none;\r\n}\r\n\r\n .tip{\r\n  float: right;\r\n}\r\n\r\n .dodaj{\r\n  float: left;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZ3JvdXBlLXBhZ2UvZ3JvdXBlLXBhZ2UuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7RUFDRSxxQkFBcUI7RUFDckIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtBQUNuQjs7Q0FFQztHQUNFLGtCQUFrQjtHQUNsQjtDQUNGOztDQUNBO0dBQ0UsV0FBVztDQUNiOztDQUVBO0dBQ0UsWUFBWTtDQUNkOztDQUNBO0dBQ0UsV0FBVztDQUNiOztDQUVBO0dBQ0UsbUJBQW1CO0NBQ3JCOztDQUVEO0VBQ0UsWUFBWTs7QUFFZDs7Q0FFQTtFQUNFLHFCQUFxQjtBQUN2Qjs7Q0FFQTtFQUNFLFlBQVk7QUFDZDs7Q0FFQTtFQUNFLFdBQVc7QUFDYiIsImZpbGUiOiJzcmMvYXBwL2dyb3VwZS1wYWdlL2dyb3VwZS1wYWdlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmxpc3RhIHtcclxuICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTQwcHg7XHJcbiAgYm9yZGVyOiAzcHggYmxhY2s7XHJcbn1cclxuXHJcbiBzcGFue1xyXG4gICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgIGZvbnQtZmFtaWx5OiAnR2lsbCBTYW5zJywgJ0dpbGwgU2FucyBNVCcsIENhbGlicmksICdUcmVidWNoZXQgTVMnLCBzYW5zLXNlcmlmXHJcbiB9XHJcbiAudHlwZXtcclxuICAgZmxvYXQ6IGxlZnQ7XHJcbiB9XHJcblxyXG4gLmRhdGV7XHJcbiAgIGZsb2F0OiByaWdodDtcclxuIH1cclxuIC51c2VybmFtZXtcclxuICAgZmxvYXQ6IGxlZnQ7XHJcbiB9XHJcblxyXG4gaDV7XHJcbiAgIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbiB9XHJcblxyXG5ocntcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgXHJcbn1cclxuXHJcbnVse1xyXG4gIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxufVxyXG5cclxuLnRpcHtcclxuICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5kb2RhantcclxuICBmbG9hdDogbGVmdDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/groupe-page/groupe-page.component.html":
/*!********************************************************!*\
  !*** ./src/app/groupe-page/groupe-page.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <!-- <div class=\"container\">\n   <div class=\"col-9\"></div>\n  <ul *ngFor=\"let n of notice\">\n    <li>\n      <div> <h5>{{n.text}}</h5>\n        <p>{{n.type.name}}</p>\n      </div>\n    </li>\n  </ul>\n  \n</div>\n\n<div>\n  <ul *ngFor=\"let t of types\">\n    <li>{{t.name}}</li>\n  </ul>\n</div> -->\n\n<!-- \n<div class=\"container\">\n\n  <div class=\"row\">\n    <div class=\"col\">\n      <button (click)=\"activateAddNotice()\">Dodaj Obestenje</button>\n      <div *ngIf=\"isCollapsed\">\n\n        <form class=\"form-horizontal\" (ngSubmit)=\"saveNotice()\">\n\n\n          <div class=\"form-group\">\n            <label class=\"control-label col-sm-1\" for=\"input-project\">Naslov Objave</label>\n            <div class=\"col-sm-11\">\n              <input class=\"form-control\" name=\"input-project\" id=\"input-title\" [(ngModel)]=\"newNotice.title\" />\n            </div>\n          </div>\n\n\n\n          <div class=\"form-group\">\n            <label class=\"control-label col-sm-1\" for=\"input-author\">Type: </label>\n            <div class=\"col-sm-11\">\n              <select class=\"form-control\" name=\"input-kategorija\" id=\"input-kategorija\" [(ngModel)]=\"newNotice.type\">\n                <option *ngFor=\"let t of types\" [ngValue]=\"k\">{{t.name}}</option>\n              </select>\n            </div>\n          </div>\n\n          <div class=\"form-group\">\n            <label t class=\"control-label col-sm-1\" for=\"input-image-url\">Tekst: </label>\n            <div class=\"col-sm-11\">\n              <input type=\"text\" class=\"form-control\" name=\"input-image-url\" id=\"input-image-url\"\n                [(ngModel)]=\"newNotice.text\" />\n            </div>\n          </div>\n          <div class=\"form-group save\">\n            <input class=\"btn btn-primary\" type=\"submit\" value=\"save\" />\n          </div>\n        </form>\n\n      </div>\n    </div>\n \n  <div class=\"col 6\">\n    <ul *ngFor=\"let n of notice\">\n      <li>\n        <div>\n          <h3>{{n.title}}</h3>\n          <h5>{{n.text}}</h5>\n          <p>{{n.type.name}}</p>\n        </div>\n      </li>\n      <br>\n    </ul>\n  </div>\n  <div class=\"col\">\n    <h3>Tip Obavestenja</h3>\n    <ul *ngFor=\"let t of types\">\n      ovde saljemo trenutno id u grupi koje se nalazimo (this.id, njega dobija iz component), a drugi parametar prosledjuje  se od tip.id, ovo nas baca na notice-of type stranicu, koja u sebi sadrzi metodu sa filtriranje-->\n      <!-- <li>\n\n        <h4><a [routerLink]=\"['/group', this.id,t.id]\">{{t.name}}</a></h4>\n      </li>\n    </ul>\n  </div>\n</div>\n</div> -->\n\n\n<div class=\"container\">\n  <div class=\"row\">\n      <div class=\"dodaj col col-lg-3\">\n          <button (click)=\"activateAddNotice()\">Dodaj Post</button>\n          <div *ngIf=\"isCollapsed\">\n    \n            <form class=\"form-horizontal\" (ngSubmit)=\"saveNotice()\">\n    \n    \n              <div class=\"form-group\">\n                <label class=\"control-label col-sm-1\" for=\"input-project\">Naslov Posta</label>\n                <div class=\"col-sm-11\">\n                  <input class=\"form-control\" name=\"input-project\" id=\"input-title\" [(ngModel)]=\"newNotice.title\" />\n                </div>\n              </div>\n    \n    \n    \n              <div class=\"form-group\">\n                <label class=\"control-label col-sm-1\" for=\"input-author\">Tip: </label>\n                <div class=\"col-sm-11\">\n                  <select class=\"form-control\" name=\"input-kategorija\" id=\"input-kategorija\" [(ngModel)]=\"newNotice.type\">\n                    <option *ngFor=\"let t of alltypes\" [ngValue]=\"t\">{{t.name}}</option>\n                  </select>\n                </div>\n              </div>\n    \n              <div class=\"form-group\">\n                <label t class=\"control-label col-sm-1\" for=\"input-text\">Tekst: </label>\n                <div class=\"col-sm-11\">\n                  <textarea type=\"text\" class=\"form-control\" name=\"input-text\" id=\"input-text\"\n                    [(ngModel)]=\"newNotice.text\"  style=\"height:200px\"></textarea>\n                </div>\n              </div>\n              <div class=\"form-group save\">\n                <input class=\"btn btn-success\" type=\"submit\" value=\"save\" />\n              </div>\n            </form>\n    \n          </div>\n        </div>\n\n        <div class=\"col col-lg-6\">\n            <ul class=\"lista\" *ngFor=\"let n of notice\">\n              <li>\n                <div>\n                \n                <span>  <a  [routerLink]=\"['/notice', n.id]\"><h3>{{n.title}}</h3></a></span>\n                  <h5>{{n.text}}</h5>\n                  <div>\n                  <span> <p class=\"username\">  By: {{n.user.username}}</p>  <p class=\"date\">Date:  {{n.date}}</p></span>\n                  <br>\n                  <p class=\"type\">Tip : {{n.type.name}}</p>\n               \n                </div>\n                </div>\n               \n              </li>\n              <hr>\n            </ul>\n            <hr>\n          </div>\n          <div class=\"tip col col-lg-3\">\n            <h3>Tip Obavestenja</h3>\n            <ul *ngFor=\"let t of types\">\n              <!-- ovde saljemo trenutno id u grupi koje se nalazimo (this.id, njega dobija iz component), a drugi parametar prosledjuje  se od tip.id, ovo nas baca na notice-of type stranicu, koja u sebi sadrzi metodu sa filtriranje-->\n              <li>\n        \n                <h4><a [routerLink]=\"['/group', this.id,t.id]\">{{t.name}}</a></h4>\n              </li>\n            </ul>\n          </div>\n\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/groupe-page/groupe-page.component.ts":
/*!******************************************************!*\
  !*** ./src/app/groupe-page/groupe-page.component.ts ***!
  \******************************************************/
/*! exports provided: GroupePageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GroupePageComponent", function() { return GroupePageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_notice_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/notice.model */ "./src/app/models/notice.model.ts");
/* harmony import */ var _notice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../notice.service */ "./src/app/notice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _type_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../type.service */ "./src/app/type.service.ts");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../security/authentication.service */ "./src/app/security/authentication.service.ts");







var GroupePageComponent = /** @class */ (function () {
    function GroupePageComponent(noticeService, route, typeService, atuhetntication) {
        this.noticeService = noticeService;
        this.route = route;
        this.typeService = typeService;
        this.atuhetntication = atuhetntication;
        this.isCollapsed = false;
        this.newNotice = new _models_notice_model__WEBPACK_IMPORTED_MODULE_2__["Notice"]({
            id: 0,
            group: {},
            title: '',
            text: '',
            date: '',
            type: {},
            user: {},
            comments: []
        });
    }
    GroupePageComponent.prototype.ngOnInit = function () {
        this.date = (new Date().getMonth() + 1).toString() + '-' + new Date().getFullYear().toString();
        this.findGroup();
        this.getUserName();
        this.completeUser();
        this.getNoticeOfGroup();
        this.getTypesOnlyFromSelectedGroup();
        this.getAllTypes();
    };
    GroupePageComponent.prototype.activateAddNotice = function () {
        this.isCollapsed = !this.isCollapsed;
    };
    GroupePageComponent.prototype.saveNotice = function () {
        var _this = this;
        var n = this.newNotice;
        console.log(n);
        this.noticeService.saveNotice(n).subscribe(function (res) {
            _this.newNotice = res;
            _this.getNoticeOfGroup();
        });
    };
    // uzima sve objave grupe koju smo selektovali id je dobio od putanje group/:id, tu smo izvukli ID i smestili ga kao parametar sa ovu funkciju
    GroupePageComponent.prototype.getNoticeOfGroup = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.noticeService.getNoticeOfGroup(_this.id).subscribe(function (res) {
                _this.notice = res;
            });
        });
    };
    // uzima samo one Tipove koje su pristuni u objavama te grupe koju smo selektovali, id je dobio od putanje group/:id, tu smo izvukli ID i smestili ga kao parametar sa ovu funkciju
    GroupePageComponent.prototype.getTypesOnlyFromSelectedGroup = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.typeService.getTypeOnlyOfThatGroup(_this.id).subscribe(function (res) {
                _this.types = res;
            });
        });
    };
    GroupePageComponent.prototype.getUserName = function () {
        this.korisnikUsername = this.atuhetntication.getUser();
    };
    GroupePageComponent.prototype.completeUser = function () {
        var _this = this;
        this.noticeService.getUser(this.korisnikUsername).subscribe(function (rets) {
            _this.user = rets;
            _this.newNotice.user = _this.user;
        });
    };
    GroupePageComponent.prototype.findGroup = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.noticeService.findGroup(_this.id).subscribe(function (res) {
                _this.group = res;
                _this.newNotice.group = _this.group;
                _this.newNotice.date = _this.date;
                _this.newNotice.comments = [];
            });
        });
    };
    GroupePageComponent.prototype.getAllTypes = function () {
        var _this = this;
        this.typeService.getAll().subscribe(function (res) {
            _this.alltypes = res;
        });
    };
    GroupePageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-groupe-page',
            template: __webpack_require__(/*! ./groupe-page.component.html */ "./src/app/groupe-page/groupe-page.component.html"),
            styles: [__webpack_require__(/*! ./groupe-page.component.css */ "./src/app/groupe-page/groupe-page.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_notice_service__WEBPACK_IMPORTED_MODULE_3__["NoticeService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _type_service__WEBPACK_IMPORTED_MODULE_5__["TypeService"], _security_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"]])
    ], GroupePageComponent);
    return GroupePageComponent;
}());



/***/ }),

/***/ "./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2thdGVnb3JpamUtbmF2aWFnYXRpb24tYmFyL2thdGVnb3JpamUtbmF2aWFnYXRpb24tYmFyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n    \n    <ul  *ngFor=\"let type of types\">\n      <li><a href=\"#\">{{type.name}}</a></li>\n    </ul>\n \n"

/***/ }),

/***/ "./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.ts ***!
  \************************************************************************************/
/*! exports provided: KategorijeNaviagationBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KategorijeNaviagationBarComponent", function() { return KategorijeNaviagationBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _type_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../type.service */ "./src/app/type.service.ts");



var KategorijeNaviagationBarComponent = /** @class */ (function () {
    function KategorijeNaviagationBarComponent(typeService) {
        this.typeService = typeService;
    }
    KategorijeNaviagationBarComponent.prototype.ngOnInit = function () {
        this.getTypes();
    };
    KategorijeNaviagationBarComponent.prototype.getTypes = function () {
        var _this = this;
        this.typeService.getAll().subscribe(function (res) {
            _this.types = res;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], KategorijeNaviagationBarComponent.prototype, "types", void 0);
    KategorijeNaviagationBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-kategorije-naviagation-bar',
            template: __webpack_require__(/*! ./kategorije-naviagation-bar.component.html */ "./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.html"),
            styles: [__webpack_require__(/*! ./kategorije-naviagation-bar.component.css */ "./src/app/kategorije-naviagation-bar/kategorije-naviagation-bar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_type_service__WEBPACK_IMPORTED_MODULE_2__["TypeService"]])
    ], KategorijeNaviagationBarComponent);
    return KategorijeNaviagationBarComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n\r\n<form class=\"form-signin\" (ngSubmit)=\"login()\">\r\n  <h2 class=\"form-signin-heading\">Please sign in</h2>\r\n  <label for=\"username\" class=\"sr-only\">Username</label>\r\n  <input type=\"text\" id=\"username\" class=\"form-control\" name=\"username\" [(ngModel)]=\"user.username\" placeholder=\"Username\" required autofocus>\r\n  <label for=\"inputPassword\" class=\"sr-only\">Password</label>\r\n  <input type=\"password\" id=\"inputPassword\" class=\"form-control\" name=\"username\" [(ngModel)]=\"user.password\" placeholder=\"Password\" required>\r\n  <button class=\"btn btn-primary btn-block\" type=\"submit\">Sign in</button>\r\n</form>\r\n<div *ngIf=wrongUsernameOrPass class=\"alert alert-warning box-msg\" role=\"alert\">\r\n  Wrong username or password.\r\n</div>\r\n</div>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../security/authentication.service */ "./src/app/security/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var LoginComponent = /** @class */ (function () {
    function LoginComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.user = {};
        this.wrongUsernameOrPass = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.authenticationService.login(this.user.username, this.user.password).subscribe(function (loggedIn) {
            if (loggedIn) {
                _this.router.navigate(['/main']);
            }
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/main/main.component.css":
/*!*****************************************!*\
  !*** ./src/app/main/main.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\r\n    list-style-type: none;\r\n    margin-bottom: 140px;\r\n    border: 3px black;\r\n  }\r\n\r\n span{\r\n   font-style: italic;\r\n   font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif\r\n }\r\n\r\n .type{\r\n    float: left;\r\n  }\r\n\r\n .date{\r\n    float: right;\r\n  }\r\n\r\n .username{\r\n    float: left;\r\n  }\r\n\r\n h5{\r\n    margin-bottom: 30px;\r\n  }\r\n\r\n hr{\r\n   color: black;\r\n   height: 2px;\r\n   \r\n }\r\n \r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9tYWluLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxxQkFBcUI7SUFDckIsb0JBQW9CO0lBQ3BCLGlCQUFpQjtFQUNuQjs7Q0FFRDtHQUNFLGtCQUFrQjtHQUNsQjtDQUNGOztDQUVBO0lBQ0csV0FBVztFQUNiOztDQUVBO0lBQ0UsWUFBWTtFQUNkOztDQUNBO0lBQ0UsV0FBVztFQUNiOztDQUVGO0lBQ0ksbUJBQW1CO0VBQ3JCOztDQUVGO0dBQ0csWUFBWTtHQUNaLFdBQVc7O0NBRWIiLCJmaWxlIjoic3JjL2FwcC9tYWluL21haW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbInVsIHtcclxuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcclxuICAgIG1hcmdpbi1ib3R0b206IDE0MHB4O1xyXG4gICAgYm9yZGVyOiAzcHggYmxhY2s7XHJcbiAgfVxyXG5cclxuIHNwYW57XHJcbiAgIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuICAgZm9udC1mYW1pbHk6ICdHaWxsIFNhbnMnLCAnR2lsbCBTYW5zIE1UJywgQ2FsaWJyaSwgJ1RyZWJ1Y2hldCBNUycsIHNhbnMtc2VyaWZcclxuIH1cclxuXHJcbiAudHlwZXtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gIH1cclxuIFxyXG4gIC5kYXRle1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gIH1cclxuICAudXNlcm5hbWV7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICB9XHJcbiBcclxuaDV7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gIH1cclxuIFxyXG5ocntcclxuICAgY29sb3I6IGJsYWNrO1xyXG4gICBoZWlnaHQ6IDJweDtcclxuICAgXHJcbiB9XHJcbiBcclxuIl19 */"

/***/ }),

/***/ "./src/app/main/main.component.html":
/*!******************************************!*\
  !*** ./src/app/main/main.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"row\">\n    <div >\n      <ul *ngFor=\"let n of notices\">\n        <li>\n          <div>\n            <span> <a [routerLink]=\"['/notice', n.id]\">\n                <h3>{{n.title}}</h3>\n              </a></span>\n            <h5>{{n.text}}</h5>\n            <div>\n              <span>\n                <p class=\"username\"> By: {{n.user.username}}</p>\n                <p class=\"date\">Date: {{n.date}}</p>\n              </span>\n              <br>\n              <p class=\"type\">Tip : {{n.type.name}}</p>\n\n            </div>\n          </div>\n          <hr>\n        </li>\n        <hr>\n      </ul>\n\n      <hr>\n    </div>\n    <div>"

/***/ }),

/***/ "./src/app/main/main.component.ts":
/*!****************************************!*\
  !*** ./src/app/main/main.component.ts ***!
  \****************************************/
/*! exports provided: MainComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainComponent", function() { return MainComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../security/authentication.service */ "./src/app/security/authentication.service.ts");
/* harmony import */ var _notice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../notice.service */ "./src/app/notice.service.ts");




var MainComponent = /** @class */ (function () {
    function MainComponent(noticeService, atuhetntication) {
        this.noticeService = noticeService;
        this.atuhetntication = atuhetntication;
    }
    MainComponent.prototype.ngOnInit = function () {
        this.getUserName();
        this.completeUser();
        this.getNotices();
    };
    MainComponent.prototype.getNotices = function () {
        var _this = this;
        this.noticeService.getNoticeOFUser(this.korisnikUsername).subscribe(function (res) {
            _this.notices = res;
            console.log(_this.user.id);
        });
    };
    MainComponent.prototype.getUserName = function () {
        this.korisnikUsername = this.atuhetntication.getUser();
    };
    MainComponent.prototype.completeUser = function () {
        var _this = this;
        this.noticeService.getUser(this.korisnikUsername).subscribe(function (rets) {
            _this.user = rets;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], MainComponent.prototype, "notices", void 0);
    MainComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main',
            template: __webpack_require__(/*! ./main.component.html */ "./src/app/main/main.component.html"),
            styles: [__webpack_require__(/*! ./main.component.css */ "./src/app/main/main.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_notice_service__WEBPACK_IMPORTED_MODULE_3__["NoticeService"], _security_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]])
    ], MainComponent);
    return MainComponent;
}());



/***/ }),

/***/ "./src/app/models/comment.mode.ts":
/*!****************************************!*\
  !*** ./src/app/models/comment.mode.ts ***!
  \****************************************/
/*! exports provided: Commentar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Commentar", function() { return Commentar; });
var Commentar = /** @class */ (function () {
    function Commentar(c) {
        this.id = c.id;
        this.parentComment = c.parentComment;
        this.commentar = c.commentar;
        this.notice = c.notice;
        this.user = c.user;
        this.date = c.date;
    }
    return Commentar;
}());



/***/ }),

/***/ "./src/app/models/notice.model.ts":
/*!****************************************!*\
  !*** ./src/app/models/notice.model.ts ***!
  \****************************************/
/*! exports provided: Notice */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Notice", function() { return Notice; });
var Notice = /** @class */ (function () {
    function Notice(ncf) {
        this.id = ncf.id;
        this.group = ncf.group;
        this.title = ncf.title;
        this.text = ncf.text;
        this.date = ncf.date;
        this.type = ncf.type;
        this.user = ncf.user;
        this.comments = ncf.comments;
    }
    return Notice;
}());



/***/ }),

/***/ "./src/app/notice-details/notice-details.component.css":
/*!*************************************************************!*\
  !*** ./src/app/notice-details/notice-details.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\r\n    list-style-type: none;\r\n  }\r\n\r\n.onecomment{\r\n    margin-bottom: 50px;\r\n}\r\n\r\nli:nth-child(2n+1) {\r\n    background:rgb(219, 219, 219);\r\n  }\r\n\r\n.onecomment.user{\r\n    font-style: italic;\r\n    font-size: 3px;\r\n    \r\n}\r\n\r\n.notice{\r\n    margin-bottom: 100px;\r\n}\r\n\r\n.comment-section{\r\n    margin-bottom: 60px;\r\n}\r\n\r\n.obrisi{\r\n    float: right;\r\n  }\r\n\r\n.indent{\r\n    text-indent: 50px;\r\n}\r\n\r\n#odgovori{\r\n    float: right;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90aWNlLWRldGFpbHMvbm90aWNlLWRldGFpbHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHFCQUFxQjtFQUN2Qjs7QUFFRjtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLDZCQUE2QjtFQUMvQjs7QUFDRjtJQUNJLGtCQUFrQjtJQUNsQixjQUFjOztBQUVsQjs7QUFFQTtJQUNJLG9CQUFvQjtBQUN4Qjs7QUFDQTtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFDQTtJQUNJLFlBQVk7RUFDZDs7QUFFRjtJQUNJLGlCQUFpQjtBQUNyQjs7QUFFQTtJQUNJLFlBQVk7QUFDaEIiLCJmaWxlIjoic3JjL2FwcC9ub3RpY2UtZGV0YWlscy9ub3RpY2UtZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidWwge1xyXG4gICAgbGlzdC1zdHlsZS10eXBlOiBub25lO1xyXG4gIH1cclxuXHJcbi5vbmVjb21tZW50e1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxufVxyXG5cclxubGk6bnRoLWNoaWxkKDJuKzEpIHtcclxuICAgIGJhY2tncm91bmQ6cmdiKDIxOSwgMjE5LCAyMTkpO1xyXG4gIH1cclxuLm9uZWNvbW1lbnQudXNlcntcclxuICAgIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuICAgIGZvbnQtc2l6ZTogM3B4O1xyXG4gICAgXHJcbn1cclxuXHJcbi5ub3RpY2V7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxMDBweDtcclxufVxyXG4uY29tbWVudC1zZWN0aW9ue1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNjBweDtcclxufVxyXG4ub2JyaXNpe1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gIH1cclxuICBcclxuLmluZGVudHtcclxuICAgIHRleHQtaW5kZW50OiA1MHB4O1xyXG59XHJcblxyXG4jb2Rnb3Zvcml7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/notice-details/notice-details.component.html":
/*!**************************************************************!*\
  !*** ./src/app/notice-details/notice-details.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"notice\" class=\"container\">\n  <div class=\"notice\">\n    <h2>{{notice.title}}</h2>\n    <div>\n      <p>{{notice.text}}</p>\n    </div>\n    <p>{{notice.date}}</p>\n  </div>\n  <div>\n    <div>\n      <h3>Komentari:</h3>\n    </div>\n    <div class=\"comment-section\">\n      <button (click)=\"toggleComment()\">Dodajte Komentar:</button>\n      <div *ngIf=\"!comment\">\n\n\n        <form (ngSubmit)=\"addComment()\">\n          <div>\n            <input class=\"form-control\" name=\"input-project\" id=\"input-commentar\" [(ngModel)]=\"newComment.commentar\" />\n            <span>\n              <input class=\"btn btn-success\" type=\"submit\" value=\"Dodaj\" />\n            </span>\n          </div>\n        </form>\n      </div>\n    </div>\n    <ul *ngFor=\"let c of notice.comments\">\n      <li>\n        <div class=\"onecomment\">\n          <p class=\"user\">By: {{c.user.username}}</p>\n          <p>{{c.commentar}}</p>\n          <p>{{c.date}}</p>\n          <p class=\"obrisi\" *ngIf=\"c.user.username === korisnikUsername\">\n            <button  class=\"btn btn-danger\"  (click)=\"deleteComment(c.id)\">Obrisi</button>\n\n          </p>\n        </div>\n        <hr>\n      </li>\n\n      <div>\n\n        <button  class=\"btn btn-info\" (click)=\"getReplies(c.id)\">Replies</button>\n      \n        \n        <div id=\"kutija1\" *ngIf=\"!repliesToggle\" class=\"indent\">\n          <ul *ngFor=\"let r of replies\">\n            <li *ngIf=\"r.parentComment === c.id\">\n              <div class=\"onecomment\">\n                <p class=\"user\">By: {{r.user.username}}</p>\n                <p>{{r.commentar}}</p>\n                <p>{{r.date}}</p>\n                <p class=\"obrisi\" *ngIf=\"r.user.username === korisnikUsername\">\n                  <button  class=\"btn btn-danger\" (click)=\"deleteComment(r.id)\">Obrisi</button>\n                </p>\n              </div>\n              <hr>\n            </li>\n          </ul>\n          <div *ngIf=\"replies[0].parentComment === c.id || 0\">\n              <form (ngSubmit)=\"addRepliy()\">\n                <div>\n                  <input class=\"form-control\" name=\"input-project\" id=\"input-commentar\"\n                    [(ngModel)]=\"commentRepliy.commentar\" />\n                  <span>\n                    <input class=\"btn btn-success\" type=\"submit\" value=\"Dodaj\" />\n                  </span>\n                </div>\n              </form>\n            </div>\n            <br>\n        </div>\n      \n      </div>\n    </ul>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/notice-details/notice-details.component.ts":
/*!************************************************************!*\
  !*** ./src/app/notice-details/notice-details.component.ts ***!
  \************************************************************/
/*! exports provided: NoticeDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticeDetailsComponent", function() { return NoticeDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_notice_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/notice.model */ "./src/app/models/notice.model.ts");
/* harmony import */ var _notice_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../notice.service */ "./src/app/notice.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _models_comment_mode__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/comment.mode */ "./src/app/models/comment.mode.ts");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../security/authentication.service */ "./src/app/security/authentication.service.ts");








var NoticeDetailsComponent = /** @class */ (function () {
    function NoticeDetailsComponent(http, noticeService, route, atuhetntication) {
        this.http = http;
        this.noticeService = noticeService;
        this.route = route;
        this.atuhetntication = atuhetntication;
        this.comment = true;
        this.repliesToggle = true;
        this.rcomment = true;
        this.newComment = new _models_comment_mode__WEBPACK_IMPORTED_MODULE_6__["Commentar"]({
            id: 0,
            commentar: '',
            notice: {},
            user: {},
            date: ''
        });
        this.commentRepliy = new _models_comment_mode__WEBPACK_IMPORTED_MODULE_6__["Commentar"]({
            id: 0,
            parentComment: 0,
            commentar: '',
            notice: {},
            user: {},
            date: ''
        });
    }
    NoticeDetailsComponent.prototype.ngOnInit = function () {
        this.date = (new Date().getMonth() + 1).toString() + '-' + new Date().getFullYear().toString();
        this.getNotice();
        this.getUserName();
        this.completeUser();
        this.newComment.date = this.date;
    };
    NoticeDetailsComponent.prototype.getNotice = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.noticeService.getOneNotice(_this.id).subscribe(function (res) {
                _this.notice = res;
                _this.newComment.notice = _this.notice;
            });
        });
    };
    NoticeDetailsComponent.prototype.getUserName = function () {
        this.korisnikUsername = this.atuhetntication.getUser();
    };
    NoticeDetailsComponent.prototype.completeUser = function () {
        var _this = this;
        this.noticeService.getUser(this.korisnikUsername).subscribe(function (rets) {
            _this.user = rets;
            _this.newComment.user = _this.user;
        });
    };
    NoticeDetailsComponent.prototype.refresh = function () {
        window.location.reload();
    };
    NoticeDetailsComponent.prototype.toggleComment = function () {
        this.comment = !this.comment;
    };
    NoticeDetailsComponent.prototype.addComment = function () {
        var _this = this;
        var c = this.newComment;
        this.noticeService.saveComment(c).subscribe(function (res) {
            _this.newComment = res;
            _this.getNotice();
            _this.refresh();
        });
    };
    NoticeDetailsComponent.prototype.addRepliy = function () {
        var _this = this;
        var c = this.commentRepliy;
        c.user = this.user;
        c.date = this.date;
        c.notice = this.notice;
        c.parentComment = this.parrentId;
        this.noticeService.saveComment(c).subscribe(function (res) {
            _this.commentRepliy = res;
            _this.getNotice();
            _this.getReplies(_this.id);
            _this.toggleReplies();
            _this.commentRepliy = new _models_comment_mode__WEBPACK_IMPORTED_MODULE_6__["Commentar"]({
                id: 0,
                parentComment: 0,
                commentar: '',
                notice: {},
                user: {},
                date: ''
            });
        });
    };
    NoticeDetailsComponent.prototype.toggleRep = function (id) {
        this.parrentId = id;
        this.rcomment = !this.rcomment;
    };
    NoticeDetailsComponent.prototype.checkUserAndPost = function () {
        if (this.notice.user === this.user) {
            return true;
        }
        return false;
    };
    NoticeDetailsComponent.prototype.deleteComment = function (id) {
        var _this = this;
        this.noticeService.deleteComment(id).subscribe(function (res) {
            _this.getNotice();
            _this.getReplies(_this.id);
            _this.toggleReplies();
        });
    };
    NoticeDetailsComponent.prototype.toggleReplies = function () {
        this.repliesToggle = !this.repliesToggle;
    };
    NoticeDetailsComponent.prototype.getReplies = function (id) {
        var _this = this;
        this.noticeService.getReplies(id).subscribe(function (res) {
            _this.toggleReplies();
            _this.replies = res;
            _this.parrentId = id;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_notice_model__WEBPACK_IMPORTED_MODULE_2__["Notice"])
    ], NoticeDetailsComponent.prototype, "notice", void 0);
    NoticeDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-notice-details',
            template: __webpack_require__(/*! ./notice-details.component.html */ "./src/app/notice-details/notice-details.component.html"),
            styles: [__webpack_require__(/*! ./notice-details.component.css */ "./src/app/notice-details/notice-details.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _notice_service__WEBPACK_IMPORTED_MODULE_3__["NoticeService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"], _security_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"]])
    ], NoticeDetailsComponent);
    return NoticeDetailsComponent;
}());



/***/ }),

/***/ "./src/app/notice-of-type/notice-of-type.component.css":
/*!*************************************************************!*\
  !*** ./src/app/notice-of-type/notice-of-type.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ul {\r\n    list-style-type: none;\r\n  }\r\n\r\n span{\r\n   font-style: italic;\r\n   font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif\r\n }\r\n\r\n .type{\r\n    float: left;\r\n  }\r\n\r\n .date{\r\n    float: right;\r\n  }\r\n\r\n .username{\r\n    float: left;\r\n  }\r\n\r\n h5{\r\n    margin-bottom: 30px;\r\n  }\r\n\r\n hr{\r\n   color: black;\r\n   \r\n }\r\n \r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90aWNlLW9mLXR5cGUvbm90aWNlLW9mLXR5cGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHFCQUFxQjtFQUN2Qjs7Q0FFRDtHQUNFLGtCQUFrQjtHQUNsQjtDQUNGOztDQUVBO0lBQ0csV0FBVztFQUNiOztDQUVBO0lBQ0UsWUFBWTtFQUNkOztDQUNBO0lBQ0UsV0FBVztFQUNiOztDQUVGO0lBQ0ksbUJBQW1CO0VBQ3JCOztDQUVEO0dBQ0UsWUFBWTs7Q0FFZCIsImZpbGUiOiJzcmMvYXBwL25vdGljZS1vZi10eXBlL25vdGljZS1vZi10eXBlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ1bCB7XHJcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcbiAgfVxyXG5cclxuIHNwYW57XHJcbiAgIGZvbnQtc3R5bGU6IGl0YWxpYztcclxuICAgZm9udC1mYW1pbHk6ICdHaWxsIFNhbnMnLCAnR2lsbCBTYW5zIE1UJywgQ2FsaWJyaSwgJ1RyZWJ1Y2hldCBNUycsIHNhbnMtc2VyaWZcclxuIH1cclxuXHJcbiAudHlwZXtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gIH1cclxuIFxyXG4gIC5kYXRle1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gIH1cclxuICAudXNlcm5hbWV7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICB9XHJcbiBcclxuaDV7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gIH1cclxuIFxyXG4gaHJ7XHJcbiAgIGNvbG9yOiBibGFjaztcclxuICAgXHJcbiB9XHJcbiBcclxuIl19 */"

/***/ }),

/***/ "./src/app/notice-of-type/notice-of-type.component.html":
/*!**************************************************************!*\
  !*** ./src/app/notice-of-type/notice-of-type.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col col-md-9\">\n        <ul *ngFor=\"let n of notices\">\n          <li>\n            <div>\n              <span>  <a  [routerLink]=\"['/notice', n.id]\"><h3>{{n.title}}</h3></a></span>\n              <h5>{{n.text}}</h5>\n              <div>\n              <span> <p class=\"username\">  By: {{n.user.username}}</p>  <p class=\"date\">Date:  {{n.date}}</p></span>\n              <br>\n              <p class=\"type\">Tip : {{n.type.name}}</p>\n           \n            </div>\n            </div>\n          </li>\n        </ul>\n        <hr>\n      </div>\n    \n    <div class=\"col col-md-3\">\n        <h3>Tip Obavestenja</h3>\n        <ul *ngFor=\"let t of types\">\n          <!-- ovde saljemo trenutno id u grupi koje se nalazimo (this.id, njega dobija iz component), a drugi parametar prosledjuje  se od tip.id, ovo nas baca na notice-of type stranicu, koja u sebi sadrzi metodu sa filtriranje-->\n          <li>\n    \n            <h4><a [routerLink]=\"['/group', this.id1,t.id]\">{{t.name}}</a></h4>\n          </li>\n        </ul>\n      </div>\n    </div>\n  \n</div>\n"

/***/ }),

/***/ "./src/app/notice-of-type/notice-of-type.component.ts":
/*!************************************************************!*\
  !*** ./src/app/notice-of-type/notice-of-type.component.ts ***!
  \************************************************************/
/*! exports provided: NoticeOfTypeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticeOfTypeComponent", function() { return NoticeOfTypeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _type_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../type.service */ "./src/app/type.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var NoticeOfTypeComponent = /** @class */ (function () {
    function NoticeOfTypeComponent(route, typeSerice) {
        var _this = this;
        this.route = route;
        this.typeSerice = typeSerice;
        route.params.subscribe(function (val) {
            _this.getNoticesOfSelectedType();
            _this.getTypesOnlyFromSelectedGroup();
        });
    }
    NoticeOfTypeComponent.prototype.ngOnInit = function () {
    };
    NoticeOfTypeComponent.prototype.getTypesOnlyFromSelectedGroup = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id1 = +params['id1'];
            _this.typeSerice.getTypeOnlyOfThatGroup(_this.id1).subscribe(function (res) {
                _this.types = res;
            });
        });
    };
    // ova dva parametra preuzimamo iz putanje koju smo dobili kad smo u group-page sistnuli tip, iz putanje group/id1/id2, preuzimamo parametre
    // id1 je ustvari Grupin id, dok je Id2 id od tipa koji smo selektovali
    NoticeOfTypeComponent.prototype.getNoticesOfSelectedType = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.id1 = +params['id1'];
            _this.id2 = +params['id2'];
            _this.typeSerice.getNoticesOfSelectedType(_this.id1, _this.id2).subscribe(function (res) {
                _this.notices = res;
            });
        });
    };
    NoticeOfTypeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-notice-of-type',
            template: __webpack_require__(/*! ./notice-of-type.component.html */ "./src/app/notice-of-type/notice-of-type.component.html"),
            styles: [__webpack_require__(/*! ./notice-of-type.component.css */ "./src/app/notice-of-type/notice-of-type.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _type_service__WEBPACK_IMPORTED_MODULE_2__["TypeService"]])
    ], NoticeOfTypeComponent);
    return NoticeOfTypeComponent;
}());



/***/ }),

/***/ "./src/app/notice.service.ts":
/*!***********************************!*\
  !*** ./src/app/notice.service.ts ***!
  \***********************************/
/*! exports provided: NoticeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoticeService", function() { return NoticeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var NoticeService = /** @class */ (function () {
    function NoticeService(http) {
        this.http = http;
    }
    NoticeService.prototype.getUserGroup = function (user) {
        return this.http.get("/api/user/" + user);
    };
    NoticeService.prototype.getUser = function (user) {
        return this.http.get("/users/" + user);
    };
    NoticeService.prototype.getNoticeOfGroup = function (id) {
        return this.http.get("/api/group/" + id);
    };
    NoticeService.prototype.saveNotice = function (notice) {
        console.log("server");
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        return this.http.post('/api/notices/', JSON.stringify(notice), { headers: headers });
    };
    NoticeService.prototype.findGroup = function (id) {
        return this.http.get("/api/onegroup/" + id);
    };
    NoticeService.prototype.getOneNotice = function (id) {
        return this.http.get("/api/notice/" + id);
    };
    NoticeService.prototype.saveComment = function (commentar) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        return this.http.post('api/notice/commentar', JSON.stringify(commentar), { headers: headers });
    };
    NoticeService.prototype.deleteComment = function (id) {
        return this.http.delete("api/comments/" + id);
    };
    NoticeService.prototype.getReplies = function (id) {
        return this.http.get("api/replies/" + id);
    };
    NoticeService.prototype.getNoticeOFUser = function (id) {
        return this.http.get("api/notice/user/" + id);
    };
    NoticeService.prototype.findByNoticeName = function (name) {
        return this.http.get("/api/notice/search/" + name);
    };
    NoticeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], NoticeService);
    return NoticeService;
}());



/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.css":
/*!*************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2Utbm90LWZvdW5kL3BhZ2Utbm90LWZvdW5kLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.html":
/*!**************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>\r\n  There is nothing here!\r\n</h1>\r\n"

/***/ }),

/***/ "./src/app/page-not-found/page-not-found.component.ts":
/*!************************************************************!*\
  !*** ./src/app/page-not-found/page-not-found.component.ts ***!
  \************************************************************/
/*! exports provided: PageNotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageNotFoundComponent", function() { return PageNotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PageNotFoundComponent = /** @class */ (function () {
    function PageNotFoundComponent() {
    }
    PageNotFoundComponent.prototype.ngOnInit = function () {
    };
    PageNotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-page-not-found',
            template: __webpack_require__(/*! ./page-not-found.component.html */ "./src/app/page-not-found/page-not-found.component.html"),
            styles: [__webpack_require__(/*! ./page-not-found.component.css */ "./src/app/page-not-found/page-not-found.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PageNotFoundComponent);
    return PageNotFoundComponent;
}());



/***/ }),

/***/ "./src/app/security/authentication.service.ts":
/*!****************************************************!*\
  !*** ./src/app/security/authentication.service.ts ***!
  \****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _security_jwt_utils_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../security/jwt-utils.service */ "./src/app/security/jwt-utils.service.ts");






var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, jwtUtilsService) {
        this.http = http;
        this.jwtUtilsService = jwtUtilsService;
        this.loginPath = '/api/login';
    }
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Content-Type': 'application/json' });
        return this.http.post(this.loginPath, JSON.stringify({ username: username, password: password }), { headers: headers })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (res) {
            var token = res && res['token'];
            if (token) {
                localStorage.setItem('currentUser', JSON.stringify({
                    username: username,
                    roles: _this.jwtUtilsService.getRoles(token),
                    token: token
                }));
                return true;
            }
            else {
                return false;
            }
        }));
    };
    AuthenticationService.prototype.getToken = function () {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        var token = currentUser && currentUser.token;
        return token ? token : "";
    };
    AuthenticationService.prototype.logout = function () {
        localStorage.removeItem('currentUser');
    };
    AuthenticationService.prototype.isLoggedIn = function () {
        if (this.getToken() != '')
            return true;
        else
            return false;
    };
    AuthenticationService.prototype.getCurrentUser = function () {
        if (localStorage.currentUser) {
            return JSON.parse(localStorage.currentUser);
        }
        else {
            return undefined;
        }
    };
    AuthenticationService.prototype.getUser = function () {
        if (localStorage.currentUser) {
            var user = JSON.parse(localStorage.getItem('currentUser'));
            return user.username;
        }
        else {
            return undefined;
        }
    };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _security_jwt_utils_service__WEBPACK_IMPORTED_MODULE_4__["JwtUtilsService"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/security/can-activate-auth.guard.ts":
/*!*****************************************************!*\
  !*** ./src/app/security/can-activate-auth.guard.ts ***!
  \*****************************************************/
/*! exports provided: CanActivateAuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CanActivateAuthGuard", function() { return CanActivateAuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../security/authentication.service */ "./src/app/security/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var CanActivateAuthGuard = /** @class */ (function () {
    function CanActivateAuthGuard(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
    }
    CanActivateAuthGuard.prototype.canActivate = function (next, state) {
        if (this.authenticationService.isLoggedIn()) {
            return true;
        }
        else {
            this.router.navigate(['/login']);
            return false;
        }
    };
    CanActivateAuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], CanActivateAuthGuard);
    return CanActivateAuthGuard;
}());



/***/ }),

/***/ "./src/app/security/jwt-utils.service.ts":
/*!***********************************************!*\
  !*** ./src/app/security/jwt-utils.service.ts ***!
  \***********************************************/
/*! exports provided: JwtUtilsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtUtilsService", function() { return JwtUtilsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var JwtUtilsService = /** @class */ (function () {
    function JwtUtilsService() {
    }
    JwtUtilsService.prototype.getRoles = function (token) {
        var jwtData = token.split('.')[1];
        var decodedJwtJsonData = window.atob(jwtData);
        var decodedJwtData = JSON.parse(decodedJwtJsonData);
        return decodedJwtData.roles.map(function (x) { return x.authority; }) || [];
    };
    JwtUtilsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], JwtUtilsService);
    return JwtUtilsService;
}());



/***/ }),

/***/ "./src/app/security/token-interceptor.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/security/token-interceptor.service.ts ***!
  \*******************************************************/
/*! exports provided: TokenInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptorService", function() { return TokenInterceptorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _security_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../security/authentication.service */ "./src/app/security/authentication.service.ts");




var TokenInterceptorService = /** @class */ (function () {
    function TokenInterceptorService(inj) {
        this.inj = inj;
    }
    TokenInterceptorService.prototype.intercept = function (request, next) {
        var authenticationService = this.inj.get(_security_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"]);
        request = request.clone({
            setHeaders: {
                'X-Auth-Token': "" + authenticationService.getToken()
            }
        });
        return next.handle(request);
    };
    TokenInterceptorService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injector"]])
    ], TokenInterceptorService);
    return TokenInterceptorService;
}());



/***/ }),

/***/ "./src/app/type.service.ts":
/*!*********************************!*\
  !*** ./src/app/type.service.ts ***!
  \*********************************/
/*! exports provided: TypeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TypeService", function() { return TypeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var TypeService = /** @class */ (function () {
    function TypeService(http) {
        this.http = http;
    }
    TypeService.prototype.getAll = function () {
        return this.http.get('/api/types');
    };
    TypeService.prototype.getTypeOnlyOfThatGroup = function (id) {
        return this.http.get("api/types/" + id + "/group");
    };
    TypeService.prototype.getNoticesOfSelectedType = function (groupId, id) {
        return this.http.get("api/group/" + groupId + "/" + id);
    };
    TypeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], TypeService);
    return TypeService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\BulletBoardProject\client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map