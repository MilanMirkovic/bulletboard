package vp.spring.rcs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.NoticeRepository;
import vp.spring.rcs.model.Notice;

@Component
public class NoticeService {

	@Autowired
	NoticeRepository noticeRepo;
	
	
	public Page<Notice> getAll(Pageable page){
		return noticeRepo.findAll(page);
	}
	
	public List<Notice> findAllList(){
		return noticeRepo.findAll();
	}
	
	public Notice findOne(Long id) {
		return noticeRepo.findById(id).orElseGet(null);
	}
	
	public Notice save(Notice notice) {
		return noticeRepo.save(notice);
	}
}
