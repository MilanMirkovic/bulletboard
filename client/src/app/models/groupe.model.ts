import { UserInterface } from './user.model';

export interface GroupeInterface {
    id?:number;
    name?:string;
    users?:UserInterface[];
}

export class Groupe implements GroupeInterface{
    id:number;
    name:string;
    users:UserInterface[];

    constructor(g:GroupeInterface){
        this.id=g.id;
        this.name=g.name;
        this.users=g.users;
    }
}