import { NoticeInterface } from './notice.model';

export interface TypeInterface {
    id?:number;
    name?:string;
    notices?:NoticeInterface[];
}



export class Typee implements TypeInterface{
    id:number;
    name:string;
    notices:NoticeInterface[];

    constructor(tc:TypeInterface){
        this.notices=tc.notices;
        this.id=tc.id;
        this.name=tc.name;
    }
}