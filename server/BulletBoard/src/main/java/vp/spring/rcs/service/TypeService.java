package vp.spring.rcs.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.NoticeRepository;
import vp.spring.rcs.data.TypeRepository;
import vp.spring.rcs.model.Notice;
import  vp.spring.rcs.model.Type;
@Component
public class TypeService {

	@Autowired
	TypeRepository typeRepo;
	
	@Autowired
	NoticeRepository noticeRepo;
	
	public List<Type> findAll(){
		return typeRepo.findAll();
	}
	
	public Type findOne(Long id) {
		return typeRepo.findById(id).orElseGet(null);
	}
	

}
