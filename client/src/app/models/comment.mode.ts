import { NoticeInterface } from './notice.model';
import { UserInterface } from './user.model';

export interface CommentarInterface{
    id:number;
    commentar:string;
    notice?:NoticeInterface;
    parentComment?:number;
    user?:UserInterface;
    date?:string;
}


export class Commentar implements CommentarInterface{
    id:number;
    parentComment:number;
    commentar:string;
    notice:NoticeInterface;
    user:UserInterface;
    date:string;

    constructor(c:CommentarInterface){
        this.id=c.id;
        this.parentComment=c.parentComment;
        this.commentar=c.commentar;
        this.notice=c.notice;
        this.user=c.user;
        this.date=c.date;
        
    }
}