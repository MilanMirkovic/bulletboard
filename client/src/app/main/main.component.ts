import { Component, OnInit, Input } from '@angular/core';
import { Notice } from '../models/notice.model';
import { UserInterface } from '../models/user.model';
import { AuthenticationService } from '../security/authentication.service';
import { NoticeService } from '../notice.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private noticeService:NoticeService, private atuhetntication:AuthenticationService) { }

  sub:any;
  user:UserInterface;
  korisnikUsername:string;

  @Input() notices:Notice[];
  ngOnInit() {
    this.getUserName();
    this.completeUser();
    this.getNotices();
  }


  getNotices(){
    this.noticeService.getNoticeOFUser(this.korisnikUsername).subscribe((res:Notice[])=>{
      this.notices=res;
      console.log(this.user.id);
    })

  }

  getUserName(){
    this.korisnikUsername= this.atuhetntication.getUser();
  
    }
  
    completeUser(){
      this.noticeService.getUser(this.korisnikUsername).subscribe((rets:UserInterface)=>{
        this.user=rets;

    
      
      })
    }


}
