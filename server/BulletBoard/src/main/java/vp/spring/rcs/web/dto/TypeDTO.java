package vp.spring.rcs.web.dto;

import java.util.HashSet;
import java.util.Set;


import vp.spring.rcs.model.Type;

public class TypeDTO {
	
	
    Long id;
	
	String name;
	
	Set<NoticeDTO> notices=new HashSet<>();


	public TypeDTO(Type type) {
		this.id=type.getId();
		this.name=type.getName();
		
	}


	public TypeDTO() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Set<NoticeDTO> getNotices() {
		return notices;
	}


	public void setNotices(Set<NoticeDTO> notices) {
		this.notices = notices;
	}
	
	
	
}
