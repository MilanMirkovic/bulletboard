package vp.spring.rcs.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.spring.rcs.data.CommentarRepository;
import vp.spring.rcs.model.Commentar;
import vp.spring.rcs.model.Notice;

@Component
public class CommentarService {

	@Autowired
	CommentarRepository commentRepo;
	
	@Autowired
	NoticeService noticeService;
	
	
	public List<Commentar> findAll(){
		return commentRepo.findAll();
	}
	
	public Commentar findOne(Long id) {
	return commentRepo.getOne(id);

	}
	public Commentar save(Commentar comment) {
		return commentRepo.save(comment);
	}

	
	public List<Commentar> findCommentsOfNotice(Long id){
		List<Commentar> getAll=findAll();
		
		List<Commentar> retVal=new ArrayList<>();
		
		Notice notice=noticeService.findOne(id);
		
			for(Commentar c : notice.getComments()) {
				if(c.getParentComment() == 0) {
				retVal.add(c);
				}
			}
			
			return retVal;
	}
	
	public void  delte(Long id) {
		 commentRepo.deleteById(id);
	}
}

