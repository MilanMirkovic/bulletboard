package vp.spring.rcs.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Commentar;
import vp.spring.rcs.model.Groupe;
import vp.spring.rcs.model.Notice;

import vp.spring.rcs.model.Type;
import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.service.CommentarService;
import vp.spring.rcs.service.GroupeService;
import vp.spring.rcs.service.KorisnikService;
import vp.spring.rcs.service.NoticeService;
import vp.spring.rcs.service.TypeService;
import vp.spring.rcs.web.dto.CommentarDTO;
import vp.spring.rcs.web.dto.GroupeDTO;
import vp.spring.rcs.web.dto.KorisnikDTO;
import vp.spring.rcs.web.dto.NoticeDTO;


@RestController
public class NoticeController {

	@Autowired
	NoticeService noticeService;

	@Autowired
	GroupeService groupService;

	@Autowired
	TypeService typeService;

	@Autowired
	KorisnikService korisnikService;

	@Autowired
	CommentarService commentService;

//	 @GetMapping(value="api/notice")
//	 public ResponseEntity<Page<NoticeDTO>> pageNotice(Pageable page){
//		 Page<Notice> notices=noticeService.getAll(page);
//		 List<NoticeDTO> dtos=notices.stream().map(n->new NoticeDTO(n)).collect(Collectors.toList());
//	 
//		 Page<NoticeDTO> retVal=new PageImpl<>(dtos,page,notices.getNumberOfElements());
//		 
//		 return new ResponseEntity<>(retVal, HttpStatus.OK);
//	 }

	@SuppressWarnings("unchecked")
	@GetMapping(value = "api/notice")
	public ResponseEntity<List<NoticeDTO>> findAll() {
		List<Notice> notices = noticeService.findAllList();
		List<Commentar> comments = commentService.findAll();

		Set<CommentarDTO> cdto = comments.stream().map(c -> new CommentarDTO(c)).collect(Collectors.toSet());
		List<NoticeDTO> dtos = notices.stream().map(n -> new NoticeDTO(n)).collect(Collectors.toList());

		for (NoticeDTO n : dtos) {
			n.setComments(cdto);
		}

		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	@GetMapping(value = "api/notice/{id}")
	public ResponseEntity<NoticeDTO> getOne(@PathVariable Long id) {
		Notice notice = noticeService.findOne(id);

		List<Commentar> comments = commentService.findCommentsOfNotice(id);

		Set<CommentarDTO> dtos = comments.stream().map(c -> new CommentarDTO(c)).collect(Collectors.toSet());
		NoticeDTO dto = new NoticeDTO(notice);

		dto.setComments(dtos);

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping(value = "api/notice/user/{id}")
	public ResponseEntity<List<NoticeDTO>> getNoticesOfCurrentUser(@PathVariable String id) {
		SecurityUser user = korisnikService.findByUsername(id);

		List<Notice> retVal = new ArrayList<>();

		List<Notice> notes = new ArrayList<>();

		for (Notice n : user.getNotices()) {
			notes.add(n);
		}
		Random rand = new Random();

		for (int i = 0; i < 4; i++) {

			int randIndex = rand.nextInt(notes.size());

			retVal.add(notes.get(randIndex));

		}

		List<NoticeDTO> dtos = notes.stream().map(n -> new NoticeDTO(n)).collect(Collectors.toList());

		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	// uzima obevestenja samo za tu gurpu u kojoj smo
	@GetMapping(value = "api/group/{id}")
	public ResponseEntity<List<NoticeDTO>> getNoticesOfOneGroup(@PathVariable Long id) {
		List<Notice> notices = groupService.NoticeOfSelectedGroupe(id);
		if (notices != null) {
			List<NoticeDTO> dtos = notices.stream().map(n -> new NoticeDTO(n)).collect(Collectors.toList());

			return new ResponseEntity<>(dtos, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.OK);
		}

	}

	// uzmia jednu grupu
	@GetMapping(value = "api/grupa/{id}")
	public ResponseEntity<GroupeDTO> getOneGroup(@PathVariable Long id) {
		Groupe group = groupService.findOne(id);

		GroupeDTO dto = new GroupeDTO(group);

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	// uzima korisnike samo selektovane grupe
	@GetMapping(value = "api/groupusers/{id}")
	public ResponseEntity<List<KorisnikDTO>> getusers(@PathVariable Long id) {
		List<SecurityUser> users = groupService.usersOfGroup(id);

		List<KorisnikDTO> dto = users.stream().map(n -> new KorisnikDTO(n)).collect(Collectors.toList());

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@PostMapping(value = "api/notices")
	public ResponseEntity<NoticeDTO> createNotice(@RequestBody NoticeDTO dto) {
		Type type = typeService.findOne(dto.getType().getId());
		SecurityUser korisnik = korisnikService.findOne(dto.getUser().getId());
		Groupe group = groupService.findOne(dto.getGroup().getId());

		Notice notice = new Notice();
		notice.setDate(dto.getDate());
		notice.setText(dto.getText());
		notice.setTitle(dto.getTitle());
		notice.setType(type);
		notice.setUser(korisnik);
		notice.setGroupe(group);

		notice = noticeService.save(notice);

		NoticeDTO noticeDTO = new NoticeDTO(notice);

		return new ResponseEntity<>(noticeDTO, HttpStatus.CREATED);
	}

	@GetMapping(value = "api/onegroup/{id}")
	public ResponseEntity<GroupeDTO> findGroup(@PathVariable Long id) {
		Groupe g = groupService.findOne(id);

		GroupeDTO dto = new GroupeDTO(g);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@GetMapping(value = "api/notice/{id}/comments")

	public ResponseEntity<List<CommentarDTO>> commentsOfNotice(@PathVariable Long id) {
		List<Commentar> comments = commentService.findCommentsOfNotice(id);

		List<CommentarDTO> dtos = comments.stream().map(c -> new CommentarDTO(c)).collect(Collectors.toList());

		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	@PostMapping(value = "api/notice/commentar")
	public ResponseEntity<CommentarDTO> createComment(@RequestBody CommentarDTO dto) {
		Commentar commentar = new Commentar();

		Notice notice = noticeService.findOne(dto.getNotice().getId());
		SecurityUser korisnik = korisnikService.findOne(dto.getUser().getId());
		commentar.setParentComment(dto.getParentComment());
		commentar.setDatum(dto.getDatum());
		commentar.setCommentar(dto.getCommentar());
		commentar.setUser(korisnik);
		commentar.setNotice(notice);

		commentar = commentService.save(commentar);
		CommentarDTO commentarDto = new CommentarDTO(commentar);

		return new ResponseEntity<>(commentarDto, HttpStatus.OK);

	}

	@GetMapping(value = "api/comments")
	public ResponseEntity<List<CommentarDTO>> getAll() {
		List<Commentar> lista = commentService.findAll();

		List<CommentarDTO> dto = lista.stream().map(c -> new CommentarDTO(c)).collect(Collectors.toList());

		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@DeleteMapping(value = "api/comments/{id}")
	public ResponseEntity<Void> deleteComment(@PathVariable Long id) {
		this.commentService.delte(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}

	@GetMapping(value = "api/replies/{id}")
	public ResponseEntity<List<CommentarDTO>> getReplies(@PathVariable Long id) {
		List<Commentar> lista = commentService.findAll();

		List<Commentar> replies = new ArrayList<>();

		for (Commentar c : lista) {
			if (c.getParentComment() == id) {
				replies.add(c);
			}

		}

		List<CommentarDTO> dto = replies.stream().map(c -> new CommentarDTO(c)).collect(Collectors.toList());

		return new ResponseEntity<>(dto, HttpStatus.OK);

	}
	

	
	@GetMapping(value="/api/notice/search/{name}")
	public ResponseEntity<List<NoticeDTO>> findByName(@PathVariable String name){
		List<Notice> notices = noticeService.findAllList();
		
		List<Notice> match=new ArrayList<>();
		
		for(Notice n : notices) {
			if(n.getTitle().toLowerCase().contains(name.toLowerCase())) {
				match.add(n);
			}
		}
		List<NoticeDTO> dtos = match.stream().map(n -> new NoticeDTO(n)).collect(Collectors.toList());
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
//
//	Kategorija novaKategorija = kategorijaService.find(projekat.getKategorija().getId());
//
//	SecurityUser user = korisnikService.getOne(projekat.getKorisnik().getId());
//	Projekat novi = new Projekat();
//	novi.setNaziv(projekat.getNaziv());
//	novi.setKategorija(novaKategorija);
//	novi.setOpis(projekat.getOpis());
//	novi.setKorisnik(user);
//
//	novi = projekatService.save(novi);
//
//	ProjekatDTO dto = new ProjekatDTO(novi);
}
