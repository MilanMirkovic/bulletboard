import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './security/authentication.service';
import { CanActivateAuthGuard } from './security/can-activate-auth.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './security/token-interceptor.service';
import { JwtUtilsService } from './security/jwt-utils.service';
import { KategorijeNaviagationBarComponent } from './kategorije-naviagation-bar/kategorije-naviagation-bar.component';
import { GroupNavBarComponent } from './group-nav-bar/group-nav-bar.component';
import { GroupePageComponent } from './groupe-page/groupe-page.component';
import { MainComponent } from './main/main.component';
import { NoticeOfTypeComponent } from './notice-of-type/notice-of-type.component';
import { NoticeDetailsComponent } from './notice-details/notice-details.component';
import { RouterModule } from '@angular/router';
import { FilterNoticesComponent } from './filter-notices/filter-notices.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    LoginComponent,
    KategorijeNaviagationBarComponent,
    GroupNavBarComponent,
    GroupePageComponent,
    MainComponent,
    NoticeOfTypeComponent,
    NoticeDetailsComponent,
    FilterNoticesComponent
  ],
  imports: [
    
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthenticationService,
    CanActivateAuthGuard,
    JwtUtilsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
