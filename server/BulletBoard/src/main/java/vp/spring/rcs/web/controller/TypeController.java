package vp.spring.rcs.web.controller;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Notice;
import vp.spring.rcs.model.Type;
import vp.spring.rcs.service.GroupeService;
import vp.spring.rcs.service.TypeService;
import vp.spring.rcs.web.dto.NoticeDTO;
import vp.spring.rcs.web.dto.TypeDTO;

@RestController
public class TypeController {

	@Autowired
	TypeService typeService;
	
	@Autowired
	GroupeService groupService;
	
	
	@GetMapping(value="/api/types")
	public ResponseEntity<List<TypeDTO>> findAll(){
		
		List<Type> types=typeService.findAll();
		
		List<TypeDTO> dtos=types.stream().map(t -> new TypeDTO(t)).collect(Collectors.toList());
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@GetMapping(value="api/types/{id}")
	public ResponseEntity<TypeDTO> findOne(@PathVariable Long id){
		
		Type type= typeService.findOne(id);
		
		TypeDTO dto=new TypeDTO(type);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
		
	}
	
	
	//filtrira beleske u odredjenoj grupi/ po tipu koji izaberemo
	@GetMapping(value="api/group/{groupId}/{id}")
	public ResponseEntity<List<NoticeDTO>> noticesOfType(@PathVariable Long groupId,@PathVariable Long id){
		
		List<Notice> notices=groupService.noticesOfType(groupId, id);
		
		List<NoticeDTO> dtos=notices.stream().map(n -> new NoticeDTO(n)).collect(Collectors.toList());
		
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	
	
	
	
	//uzima tipove selektovane grupe
	@GetMapping(value="api/types/{id}/group")
	public ResponseEntity<Set<TypeDTO>> typeForOneGroup(@PathVariable Long id){
		
	Set<Type> types=groupService.TypesOfOneGroup(id);
		
		Set<TypeDTO> dtos= types.stream().map(n -> new TypeDTO(n)).collect(Collectors.toSet());
		
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	}
