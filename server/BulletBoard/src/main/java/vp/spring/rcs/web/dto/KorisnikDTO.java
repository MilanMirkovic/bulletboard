package vp.spring.rcs.web.dto;

import java.util.HashSet;
import java.util.Set;

import vp.spring.rcs.model.Commentar;
import vp.spring.rcs.model.Groupe;
import vp.spring.rcs.model.Notice;
import vp.spring.rcs.model.user.SecurityUser;

public class KorisnikDTO {

	private Long id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	
	Set<GroupeDTO> groups=new HashSet<>();
	Set<NoticeDTO> notices=new HashSet<>();
	
	
	Set<CommentarDTO> comments=new HashSet<>();
	
	public KorisnikDTO(SecurityUser user) {
		this.id=user.getId();
		this.username=user.getUsername();
		this.password=user.getPassword();
		this.firstName=user.getFirstName();
		this.lastName=user.getLastName();
	
	}

	public KorisnikDTO() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<GroupeDTO> getGroups() {
		return groups;
	}

	public void setGroups(Set<GroupeDTO> groups) {
		this.groups = groups;
	}

	public Set<NoticeDTO> getNotices() {
		return notices;
	}

	public void setNotices(Set<NoticeDTO> notices) {
		this.notices = notices;
	}

	public Set<CommentarDTO> getComments() {
		return comments;
	}

	public void setComments(Set<CommentarDTO> comments) {
		this.comments = comments;
	}
	
	
}
