import { Component, OnInit } from '@angular/core';
import { Notice } from '../models/notice.model';
import { NoticeService } from '../notice.service';
import { ActivatedRoute } from '@angular/router';
import { Typee } from '../models/type.mode';
import { TypeService } from '../type.service';
import { UserInterface } from '../models/user.model';
import { AuthenticationService } from '../security/authentication.service';
import { GroupeInterface } from '../models/groupe.model';
import { TouchSequence } from 'selenium-webdriver';
import { CommentarInterface } from '../models/comment.mode';

@Component({
  selector: 'app-groupe-page',
  templateUrl: './groupe-page.component.html',
  styleUrls: ['./groupe-page.component.css']
})
export class GroupePageComponent implements OnInit {

  group:GroupeInterface;
  sub:any;
  id:number;
  
  korisnikUsername:string;
  types:Typee[];
  notice:Notice[];
 
 alltypes:Typee[];
  user:UserInterface;
  newNotice:Notice;
  isCollapsed:boolean=false;
  date:string;

  
  constructor(private noticeService:NoticeService,private route:ActivatedRoute,private typeService:TypeService, private atuhetntication:AuthenticationService) { 
    this.newNotice=new Notice({
      id:0,
      group:{},
      title:'',
      text:'',
      date:'',
      type:{},
      user:{},
      comments:[]
    })

    
   
  }

  ngOnInit() {
    this.date = (new Date().getMonth() + 1).toString() + '-' + new Date().getFullYear().toString();
    this.findGroup();
    this.getUserName();
    this.completeUser();
    this.getNoticeOfGroup();
    this.getTypesOnlyFromSelectedGroup();
    this.getAllTypes();
  }

activateAddNotice(){
this.isCollapsed=!this.isCollapsed;
}

saveNotice(){
  let n=this.newNotice;
  console.log(n)
  this.noticeService.saveNotice(n).subscribe((res:Notice)=>{
  this.newNotice=res;
  this.getNoticeOfGroup();
  })
}

  // uzima sve objave grupe koju smo selektovali id je dobio od putanje group/:id, tu smo izvukli ID i smestili ga kao parametar sa ovu funkciju
  getNoticeOfGroup(){
    this.sub=this.route.params.subscribe(params=>{
      this.id =+params['id'];
      this.noticeService.getNoticeOfGroup(this.id).subscribe((res:Notice[])=>{
        this.notice=res;
      })
    })
    
  }
// uzima samo one Tipove koje su pristuni u objavama te grupe koju smo selektovali, id je dobio od putanje group/:id, tu smo izvukli ID i smestili ga kao parametar sa ovu funkciju
  getTypesOnlyFromSelectedGroup(){
    this.sub=this.route.params.subscribe(params=>{
      this.id =+params['id'];
    this.typeService.getTypeOnlyOfThatGroup(this.id).subscribe((res:Typee[])=>{
      this.types=res;
    })
  })
}

getUserName(){
  this.korisnikUsername= this.atuhetntication.getUser();

  }

  completeUser(){
    this.noticeService.getUser(this.korisnikUsername).subscribe((rets:UserInterface)=>{
      this.user=rets;
      this.newNotice.user=this.user;
  
    
    })
  }

  findGroup(){
    this.sub=this.route.params.subscribe(params=>{
      this.id =+params['id'];
    this.noticeService.findGroup(this.id).subscribe((res:GroupeInterface)=>{
      this.group=res;
      this.newNotice.group=this.group;
      this.newNotice.date=this.date;
      this.newNotice.comments=[];
      
    });
  })
}
getAllTypes(){
  this.typeService.getAll().subscribe((res:Typee[])=>{
    this.alltypes=res;
  })
}



}
