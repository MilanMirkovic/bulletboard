package vp.spring.rcs.model.user;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import vp.spring.rcs.model.Commentar;
import vp.spring.rcs.model.Groupe;
import vp.spring.rcs.model.Notice;

@Entity
public class SecurityUser {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String username;
	
	private String password;
	
	private String firstName;
	
	private String lastName;
	
	
	@ManyToMany(mappedBy="users")
	Set<Groupe> groups=new HashSet<>();
	
	@OneToMany(mappedBy="user")
	Set<Notice> notices=new HashSet<>();
	
	@OneToMany(mappedBy="user")
	Set<Commentar> comments=new HashSet<>();
	
	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<SecurityUserAuthority> userAuthorities = new HashSet<SecurityUserAuthority>();
	
	
	
	
	

	public SecurityUser() {
		super();
	}




	public SecurityUser(Long id, String username, String password, String firstName, String lastName, Set<Groupe> groups,
			Set<Notice> notices, Set<Commentar> comments, Set<SecurityUserAuthority> userAuthorities) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.groups = groups;
		this.notices = notices;
		this.comments = comments;
		this.userAuthorities = userAuthorities;
	}
	
	
	

	public Set<Groupe> getGroups() {
		return groups;
	}




	public void setGroups(Set<Groupe> groups) {
		this.groups = groups;
	}




	public Set<Notice> getNotices() {
		return notices;
	}




	public void setNotices(Set<Notice> notices) {
		this.notices = notices;
	}




	public Set<Commentar> getComments() {
		return comments;
	}




	public void setComments(Set<Commentar> comments) {
		this.comments = comments;
	}




	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<SecurityUserAuthority> getUserAuthorities() {
		return userAuthorities;
	}

	public void setUserAuthorities(Set<SecurityUserAuthority> userAuthorities) {
		this.userAuthorities = userAuthorities;
	}
}
