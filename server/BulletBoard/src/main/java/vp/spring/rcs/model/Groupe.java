package vp.spring.rcs.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
@Table(catalog="bulletboard")
public class Groupe {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	
	String name;
	
	@OneToMany(mappedBy="groupe")
	Set<Notice> notices=new HashSet<>();
	
	
	@ManyToMany
	Set<SecurityUser> users=new HashSet<>();

	public Groupe(Long id,  String name) {
		super();
		this.name=name;
		this.id = id;
		
	}

	public Groupe() {
		super();
	}

	
	
	public Set<Notice> getNotices() {
		return notices;
	}

	public void setNotices(Set<Notice> notices) {
		this.notices = notices;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<SecurityUser> getUsers() {
		return users;
	}

	public void setUsers(Set<SecurityUser> users) {
		this.users = users;
	}
	
	

}
