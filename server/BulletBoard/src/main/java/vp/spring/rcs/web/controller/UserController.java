package vp.spring.rcs.web.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.rcs.model.Groupe;
import vp.spring.rcs.model.user.SecurityUser;
import vp.spring.rcs.security.TokenUtils;
import vp.spring.rcs.service.KorisnikService;
import vp.spring.rcs.web.dto.GroupeDTO;
import vp.spring.rcs.web.dto.KorisnikDTO;
import vp.spring.rcs.web.dto.LoginDTO;
import vp.spring.rcs.web.dto.TokenDTO;

@RestController
public class UserController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	TokenUtils tokenUtils;

	@Autowired
	KorisnikService korisnikService;

	@RequestMapping(value = "/api/login", method = RequestMethod.POST)
	public ResponseEntity<TokenDTO> login(@RequestBody LoginDTO loginDTO) {
		try {
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginDTO.getUsername(),
					loginDTO.getPassword());
			Authentication authentication = authenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			UserDetails details = userDetailsService.loadUserByUsername(loginDTO.getUsername());
			String genToken = tokenUtils.generateToken(details);
			return new ResponseEntity<TokenDTO>(new TokenDTO(genToken), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<TokenDTO>(new TokenDTO(""), HttpStatus.BAD_REQUEST);
		}
	}
//
//	@GetMapping(value = "/api/user/{username}")
//	public ResponseEntity<KorisnikDTO> findByUserName(@PathVariable String username) {
//		SecurityUser korisnik = korisnikService.findByUserName(username);
//		Set<GroupeDTO> group = new HashSet<>();
//
//		KorisnikDTO kdto = new KorisnikDTO(korisnik);
//
//		for (Groupe g : korisnik.getGroups()) {
//			GroupeDTO dto = new GroupeDTO(g);
//			group.add(dto);
//
//		}
//		kdto.setGroups(group);
//
//		return new ResponseEntity<>(kdto, HttpStatus.OK);
//	}
	@GetMapping(value = "/api/user/{username}")
	public ResponseEntity<Set<GroupeDTO>> findUserGroup(@PathVariable String username) {
		SecurityUser korisnik = korisnikService.findByUsername(username);
		Set<GroupeDTO> group = new HashSet<>();

		KorisnikDTO kdto = new KorisnikDTO(korisnik);
		
		for (Groupe g : korisnik.getGroups()) {
			GroupeDTO dto = new GroupeDTO(g);
			group.add(dto);

		}
		kdto.setGroups(group);

		return new ResponseEntity<>(group, HttpStatus.OK);
	}
	

	
	@GetMapping(value="/users/{username}")
	public ResponseEntity<KorisnikDTO> findByUserName(@PathVariable String username){
		SecurityUser korisnik=korisnikService.findByUsername(username);
		
		KorisnikDTO dto=new KorisnikDTO(korisnik);
		
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	
}
