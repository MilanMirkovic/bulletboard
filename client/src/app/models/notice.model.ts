import { TypeInterface } from './type.mode';
import { CommentarInterface } from './comment.mode';
import { UserInterface } from './user.model';
import { GroupeInterface } from './groupe.model';

export interface NoticeInterface{
    id?:number;
    group?:GroupeInterface;
    title?:string;
    text?:string;
    date?:string;
    type?:TypeInterface;
    user?:UserInterface;
    
    comments?:CommentarInterface[];


}


export class Notice implements NoticeInterface{ 
    id:number;
    group:GroupeInterface;
    title:string;
    text:string;
    date:string;
    type:TypeInterface;
    user:UserInterface;
    comments:CommentarInterface[];

    constructor(ncf:NoticeInterface){
        this.id=ncf.id;
        this.group=ncf.group;
        this.title=ncf.title;
        this.text=ncf.text;
        this.date=ncf.date;
        this.type=ncf.type;
        this.user=ncf.user;
        this.comments=ncf.comments;
    }
}