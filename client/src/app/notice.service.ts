import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserInterface, User } from './models/user.model';
import { Observable } from 'rxjs';
import { Groupe, GroupeInterface } from './models/groupe.model';
import { Notice } from './models/notice.model';
import { Commentar } from './models/comment.mode';

@Injectable({
  providedIn: 'root'
})
export class NoticeService {

  constructor(private http:HttpClient) { }


  getUserGroup(user:string): Observable<Groupe[]>{
    return this.http.get<Groupe[]>(`/api/user/${user}`);
  }

  getUser(user:string): Observable<UserInterface>{
    return this.http.get<UserInterface>(`/users/${user}`);
  }
  getNoticeOfGroup(id:number): Observable<Notice[]>{
  return this.http.get<Notice[]>(`/api/group/${id}`);
  }

  saveNotice(notice:Notice): Observable<Notice>{
    console.log("server");
    let headers=new HttpHeaders({'Content-Type':'application/json'});
  return this.http.post<Notice>('/api/notices/', JSON.stringify(notice), {headers});
  
  }

  findGroup(id:number):Observable<GroupeInterface>{
    return this.http.get<GroupeInterface>(`/api/onegroup/${id}`);
  }

  getOneNotice(id:number):Observable<Notice>{
    return this.http.get<Notice>(`/api/notice/${id}`);
  }

  saveComment(commentar:Commentar): Observable<Commentar>{
    let headers=new HttpHeaders({'Content-Type':'application/json'});
    return this.http.post<Commentar>('api/notice/commentar', JSON.stringify(commentar), {headers});
  }

 deleteComment(id:number):Observable<any>{
  return this.http.delete<any>(`api/comments/${id}`);
 }
 

 getReplies(id:number): Observable<Commentar[]>{
   return this.http.get<Commentar[]>(`api/replies/${id}`);
 }

 getNoticeOFUser(id:string):Observable<Notice[]>{
   return this.http.get<Notice[]>(`api/notice/user/${id}`);
 }

 findByNoticeName(name:string): Observable<Notice[]>{
   return this.http.get<Notice[]>(`/api/notice/search/${name}`);
 }
}

