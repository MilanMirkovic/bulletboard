package vp.spring.rcs.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
@Table(catalog="bulletboard")
public class Notice {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	
	
	String title;
	String text;
	
	@ManyToOne
	Groupe groupe;
	
	String date;
	
	@ManyToOne
	Type type;
	
	@ManyToOne
	SecurityUser user;
	
	@OneToMany(mappedBy="notice",  cascade = CascadeType.ALL)
	Set<Commentar> comments=new HashSet<Commentar>();

	public Notice(Long id, String text, String date, Type type, SecurityUser user,String title, Set<Commentar> comments) {
		super();
		this.id = id;
		this.title=title;
		this.text = text;
		this.date = date;
		this.type = type;
		this.user = user;
		this.comments = comments;
	}

	
	public Groupe getGroupe() {
		return groupe;
	}


	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}


	public Notice(Long id, String title, String text, Groupe groupe, String date, Type type, SecurityUser user,
			Set<Commentar> comments) {
		super();
		this.id = id;
		this.title = title;
		this.text = text;
		this.groupe = groupe;
		this.date = date;
		this.type = type;
		this.user = user;
		this.comments = comments;
	}


	public Notice() {
	
	}
	
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public SecurityUser getUser() {
		return user;
	}

	public void setUser(SecurityUser user) {
		this.user = user;
	}

	public Set<Commentar> getComments() {
		return comments;
	}

	public void setComments(Set<Commentar> comments) {
		this.comments = comments;
	}
	
	
	
}
