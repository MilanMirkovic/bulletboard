import { Component, OnInit, Input } from '@angular/core';
import { Notice } from '../models/notice.model';
import { ActivatedRoute } from '@angular/router';
import { NoticeService } from '../notice.service';

@Component({
  selector: 'app-filter-notices',
  templateUrl: './filter-notices.component.html',
  styleUrls: ['./filter-notices.component.css']
})
export class FilterNoticesComponent implements OnInit {

  constructor(private route:ActivatedRoute, private noticeService:NoticeService) { }

  name:string;
  sub:any;
  @Input() notices:Notice[];
  ngOnInit() {
    this.getNotices();
  }



  getNotices(){
    this.sub= this.route.params.subscribe(params=>{
      this.name=params['name'];
      this.noticeService.findByNoticeName(this.name).subscribe((res:Notice[])=>{
        this.notices=res;
      })
    })
  }
}
