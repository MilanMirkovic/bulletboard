import { Component, OnInit, Input } from '@angular/core';
import { Notice } from '../models/notice.model';
import { NoticeService } from '../notice.service';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Commentar } from '../models/comment.mode';
import { UserInterface } from '../models/user.model';
import { AuthenticationService } from '../security/authentication.service';

@Component({
  selector: 'app-notice-details',
  templateUrl: './notice-details.component.html',
  styleUrls: ['./notice-details.component.css']
})
export class NoticeDetailsComponent implements OnInit {

  id:number;
  private sub:any;
  
  newComment:Commentar;

  @Input() notice:Notice;

  comment:boolean=true;
  date:string;
  user:UserInterface;
  korisnikUsername:string;
  commentar:string;
  parrentId:number;
  replies:Commentar[];

  repliesToggle:boolean=true;
  rcomment:boolean=true;

  commentRepliy:Commentar;

  constructor(private http: HttpClient, private noticeService:NoticeService, private route:ActivatedRoute,private atuhetntication:AuthenticationService) { 
    this.newComment=new Commentar({
      id:0,
      commentar:'',
      notice:{},
      user:{},
      date:''
    })

    this.commentRepliy=new Commentar({
      id:0,
      parentComment:0,
      commentar:'',
      notice:{},
      user:{},
      date:''
    })
  }

  ngOnInit() {
    this.date = (new Date().getMonth() + 1).toString() + '-' + new Date().getFullYear().toString();
    this.getNotice();
    this.getUserName();
    this.completeUser();
    this.newComment.date=this.date;
  }


  getNotice(){
    this.sub= this.route.params.subscribe(params => {
    this.id=+params['id'];
    this.noticeService.getOneNotice(this.id).subscribe((res:Notice)=>{
      this.notice=res;
      this.newComment.notice=this.notice;
    })
    
    })
  }
  getUserName(){
    this.korisnikUsername= this.atuhetntication.getUser();
  
    }
  
    completeUser(){
      this.noticeService.getUser(this.korisnikUsername).subscribe((rets:UserInterface)=>{
        this.user=rets;
        this.newComment.user=this.user;
    
      
      })
    }

    refresh(): void {
      window.location.reload();
  }


  toggleComment(){
    this.comment=!this.comment;
  }

  addComment(){
    let c=this.newComment;
    this.noticeService.saveComment(c).subscribe((res:Commentar)=>{
      this.newComment=res;
      this.getNotice();
      this.refresh();
    })
  }

  addRepliy(){
    
    let c=this.commentRepliy;
    c.user=this.user;
    c.date=this.date;
    c.notice=this.notice;
    
    c.parentComment=this.parrentId;
    this.noticeService.saveComment(c).subscribe((res:Commentar)=>{
      this.commentRepliy=res;
      this.getNotice();
      this.getReplies(this.id);
      this.toggleReplies();
      this.commentRepliy=new Commentar({
        id:0,
        parentComment:0,
        commentar:'',
        notice:{},
        user:{},
        date:''
      })
    })
  }

  toggleRep(id:number){
    this.parrentId=id;
    this.rcomment=!this.rcomment;

  }
  checkUserAndPost(): boolean{
    if(this.notice.user === this.user){
      return true;
    }return false;
  }

  deleteComment(id:number){
    this.noticeService.deleteComment(id).subscribe((res:any)=>{
      this.getNotice();
      this.getReplies(this.id);
      this.toggleReplies();
    })
  }


  toggleReplies(){
    this.repliesToggle=!this.repliesToggle;
  }
  
  getReplies(id:number){
    this.noticeService.getReplies(id).subscribe((res:Commentar[])=>{
      this.toggleReplies();
      this.replies=res;
      this.parrentId=id;
     
  
      
    });
  }
}
