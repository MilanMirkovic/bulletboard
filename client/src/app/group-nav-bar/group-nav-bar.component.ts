import { Component, OnInit,Input } from '@angular/core';


import { TypeService } from '../type.service';
import {  Typee } from '../models/type.mode';
import { AuthenticationService } from '../security/authentication.service';
import { UserInterface, User } from '../models/user.model';
import { NoticeService } from '../notice.service';
import { Groupe } from '../models/groupe.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-group-nav-bar',
  templateUrl: './group-nav-bar.component.html',
  styleUrls: ['./group-nav-bar.component.css']
})
export class GroupNavBarComponent implements OnInit {

  @Input() 
  types:Typee[];
  name:string;
  korisnikUsername:string;
  user:User;
  groups:Groupe[];
  constructor(private typeService:TypeService,private atuhetntication:AuthenticationService, private noticeService: NoticeService,private router: Router) { }

  ngOnInit() {
    this.getTypes();
    this.getUserName();
    this.completeUser();
  }

  
  getTypes(){
    this.typeService.getAll().subscribe((res:Typee[])=>{
      this.types=res;
    })

  }

  getUserName(){
    this.korisnikUsername= this.atuhetntication.getUser();
  }
  
    

    // vraca samo one grupe koji je trenutno ulogovani user clan
    completeUser(){
      this.noticeService.getUserGroup(this.korisnikUsername).subscribe((rets:Groupe[])=>{
        this.groups=rets;
      
      })
    }



    logout():void{
      this.atuhetntication.logout();
      this.router.navigate(['/login']);
    }
  
    isLoggedIn():boolean{
      return this.atuhetntication.isLoggedIn();
    }

}
