package vp.spring.rcs.web.dto;

import java.util.HashSet;
import java.util.Set;

import vp.spring.rcs.model.Notice;

public class NoticeDTO {

Long id;
	
	String text;
	
	String date;
	String title;
	TypeDTO type;
	
	GroupeDTO group;
	
	KorisnikDTO user;

	Set<CommentarDTO> comments=new HashSet<CommentarDTO>();
	
	public NoticeDTO(Notice notice) {
		this.id=notice.getId();
		
		this.title=notice.getTitle();
		this.date=notice.getDate();
		this.text=notice.getText();
		this.group=new GroupeDTO(notice.getGroupe());
		this.type=new TypeDTO(notice.getType());
		this.user=new KorisnikDTO(notice.getUser());
	}
	
	

	public NoticeDTO() {
		super();
	}

	


	public GroupeDTO getGroup() {
		return group;
	}



	public void setGroup(GroupeDTO group) {
		this.group = group;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public TypeDTO getType() {
		return type;
	}

	public void setType(TypeDTO type) {
		this.type = type;
	}

	public KorisnikDTO getUser() {
		return user;
	}

	public void setUser(KorisnikDTO user) {
		this.user = user;
	}

	public Set<CommentarDTO> getComments() {
		return comments;
	}

	public void setComments(Set<CommentarDTO> comments) {
		this.comments = comments;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
}
