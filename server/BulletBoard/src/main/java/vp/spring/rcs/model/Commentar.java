package vp.spring.rcs.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import vp.spring.rcs.model.user.SecurityUser;

@Entity
@Table(catalog="bulletboard")
public class Commentar {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	Long id;
	
	String commentar;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	Notice  notice;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	SecurityUser user;
	
	int parentComment;


	String datum;

	public Commentar(Long id, String commentar, Notice notice,int parrentComment, SecurityUser user, String datum) {
		
		super();
		this.parentComment=parrentComment;
		this.datum=datum;
		this.id = id;
		this.commentar = commentar;
		this.notice = notice;
		this.user = user;
	}
	

	
	public int getParentComment() {
		return parentComment;
	}



	public void setParentComment(int parentComment) {
		this.parentComment = parentComment;
	}



	public Commentar() {
	
	}

	

	public String getDatum() {
		return datum;
	}


	public void setDatum(String datum) {
		this.datum = datum;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommentar() {
		return commentar;
	}

	public void setCommentar(String commentar) {
		this.commentar = commentar;
	}

	public Notice getNotice() {
		return notice;
	}

	public void setNotice(Notice notice) {
		this.notice = notice;
	}

	public SecurityUser getUser() {
		return user;
	}

	public void setUser(SecurityUser user) {
		this.user = user;
	}
	
	

}
