import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoticeOfTypeComponent } from './notice-of-type.component';

describe('NoticeOfTypeComponent', () => {
  let component: NoticeOfTypeComponent;
  let fixture: ComponentFixture<NoticeOfTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoticeOfTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoticeOfTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
