import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterNoticesComponent } from './filter-notices.component';

describe('FilterNoticesComponent', () => {
  let component: FilterNoticesComponent;
  let fixture: ComponentFixture<FilterNoticesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterNoticesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterNoticesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
