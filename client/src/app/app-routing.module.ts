import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateAuthGuard } from './security/can-activate-auth.guard';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GroupNavBarComponent } from './group-nav-bar/group-nav-bar.component';
import { GroupePageComponent } from './groupe-page/groupe-page.component';
import { MainComponent } from './main/main.component';
import { NoticeOfTypeComponent } from './notice-of-type/notice-of-type.component';
import { NoticeDetailsComponent } from './notice-details/notice-details.component';
import { FilterNoticesComponent } from './filter-notices/filter-notices.component';

const routes: Routes = [
  // { path: 'record/:id', component: RecordDetailsComponent, canActivate:[CanActivateAuthGuard] },
  {path:'notices/:name', component:FilterNoticesComponent},
  {path: 'notice/:id', component: NoticeDetailsComponent},
   { path: 'main', component:MainComponent}, 
   { path: 'login', component: LoginComponent}, 
   { path: 'group/:id', component: GroupePageComponent}, 
     { path: 'group/:id1/:id2', component: NoticeOfTypeComponent},
     
  // { path: '', redirectTo: 'main', pathMatch: 'full' },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
