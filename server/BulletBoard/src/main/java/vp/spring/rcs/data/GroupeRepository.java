package vp.spring.rcs.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Component;

import vp.spring.rcs.model.Groupe;
import vp.spring.rcs.model.Notice;

@Component
public interface GroupeRepository extends JpaRepository<Groupe, Long>{

	public Groupe findByName(String name);

}
