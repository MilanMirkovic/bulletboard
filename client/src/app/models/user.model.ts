import { NoticeInterface } from './notice.model';
import { CommentarInterface } from './comment.mode';
import { GroupeInterface } from './groupe.model';

export interface UserInterface{
    id?:number;
    name?:string;
    surname?:string;
    username?:string;
    password?:string;
    notices?:NoticeInterface[];
    comments?:CommentarInterface[];
    groupe?:GroupeInterface[];


}

export class User implements UserInterface{
    id:number;
    name:string;
    surname:string;
    username:string;
    password:string;
    notices:NoticeInterface[];
    comments:CommentarInterface[];
    groupe:GroupeInterface[];


    constructor(ucf:UserInterface){
        this.id=ucf.id;
        this.name=ucf.name;
        this.surname=ucf.surname;
        this.username=ucf.username;
        this.password=ucf.password;
        this.notices=ucf.notices;
        this.comments=ucf.comments;
        this.groupe=ucf.groupe;
    }

}