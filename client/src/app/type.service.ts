import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Typee } from './models/type.mode';
import { Notice } from './models/notice.model';



@Injectable({
  providedIn: 'root'
})
export class TypeService {

  constructor(private http: HttpClient) { }



  getAll():Observable<Typee[]>{
    return this.http.get<Typee[]>('/api/types');
  }


  getTypeOnlyOfThatGroup(id:number):Observable<Typee[]>{
      return this.http.get<Typee[]>(`api/types/${id}/group`);
  }


  getNoticesOfSelectedType(groupId:number, id:number): Observable<Notice[]>{
    return this.http.get<Notice[]>(`api/group/${groupId}/${id}`);
  }


}
